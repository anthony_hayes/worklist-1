module.exports = {
  // './js/worklist-ng/worklist.app.js', './js/worklist-ng/worklist.workitemactions.mdl.js'
  entry: ['./module.js'],
  output: {
    filename: './dist/worklist-module.js'
  },
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
  },
  modulesDirectories: [
    'js/worklist-ng'
  ],
  devtool: 'source-map'
}
