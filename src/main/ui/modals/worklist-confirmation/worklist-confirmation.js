module.exports = (function() {

'use strict';

angular.module('worklist')

	.controller('WorkListConfirmationCtrl', ['$scope', '$modalInstance', '$log', 'details', function ($scope, $modalInstance, $log, details) {
	  $scope.details = details;
	  $scope.yes = function () {
	    $modalInstance.close();
	  };

	  $scope.no = function () {
	    $modalInstance.dismiss('cancel');
	  };
	}])
	
})();
