module.exports = (function() {

'use strict';

angular.module('worklist')


	/* Link to account modal */
	.controller('WorkItemLinkModalCtrl', ['$scope', '$modalInstance', '$log','WorkListViewSrv','WorkItemActionsSrv', 'workitem','action',
	                                      	function ($scope, $modalInstance, $log, WorkListViewSrv, WorkItemActionsSrv, workitem, action) {
	  $scope.accounts = [];
	  $scope.searchVal = "";
	  $scope.showSearchAccounts = false;
	  $scope.displayResults = false;

	  $scope.select = function (account) {
	    $modalInstance.close(account);
	  };

	  $scope.close = function () {
	    $modalInstance.dismiss('cancel');
	  };

	  $scope.createNewAccount = function(){
	    	action.url = action.url + '&TARGET_ACCOUNT=-1';
	    	WorkItemActionsSrv.executeAction(workitem,action,function(res){
	    		var url = res.data.response.results.url;
	    		window.location = url;
	    	});
	  };

	  $scope.lookupAccounts = function(){
		  if($scope.searchVal.length < 3){
			  $scope.accounts =  [];
			  $scope.displayResults = false;
			  return;
		  }

		  WorkListViewSrv.searchEntities('account',$scope.searchVal,null,function(data){
			  $scope.accounts =  data.response.results.docs;
			  $scope.displayResults = true;
		  });
	  };
	}])
})();
