module.exports = (function() {

'use strict';

angular.module('worklist')

	/* saved filter delete controller */
	.controller('SavedFilterDeleteCtrl', ['$scope', '$modalInstance', '$log', 'savedSearch', 'getMetaData', 'WorkListViewSrv', function ($scope, $modalInstance, $log, savedSearch, getMetaData, WorkListViewSrv) {

	  $scope.savedSearch = savedSearch;
	  $scope.getMetaData = getMetaData;
	  $scope.WorkListViewSrv = WorkListViewSrv;
	  $scope.no = function () {
	    $modalInstance.dismiss('cancel');
	  };

	  $scope.yes = function () {
		  WorkListViewSrv.deleteSearch(savedSearch, getMetaData);
		  $modalInstance.dismiss('cancel');
	  };
	}])
})();
