module.exports = (function() {
'use strict';

	angular.module('worklist')

	.service('WorkItemActionsSrv', ['apiGatewayService', function(apiGatewayService){

		var self = this;

		self.getWorkItemActions = function(workitemId, successCallBack, errorCallBack){

			 return apiGatewayService.get("workerscomp/workflow/WorkItemActions",null, {
				 "channel": "AGENT",
				 "WORKITEMID": workitemId
			 }).then(successCallBack, errorCallBack);
		};

		self.executeAction = function(workitem, action, successCallBack, errorCallBack){
		 return apiGatewayService.get(action.url, null, null).then(successCallBack, errorCallBack);
		}
	}])


})();
