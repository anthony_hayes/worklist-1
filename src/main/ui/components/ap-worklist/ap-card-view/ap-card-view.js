module.exports = (function() {
  require('./ap-card-ctrl.js');
  require('./ap-workitem-card/ap-workitem-card.js');

  'use strict';

  angular.module('worklist')

  .component('apCardView', {
    controller: 'ap.worklist.worklistDataCtrl',
    template: require('raw!./ap-card-view.html'),
    require: {
			apWorklist:"^apWorklist"
		}
  })

})();
