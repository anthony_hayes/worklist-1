(function() {

    'use strict';

    angular.module('worklist')

    .component('apWorklistQuoteBoxes', {
        controller: ['$resource', '$filter', 'contextPath', 'model', 'WorkListViewSrv', '$log', 'apiGatewayService', WorklistQuoteBoxController],
        template: require('raw!./ap-worklist-quote-boxes.html'),
        bindings: {
          "fetchWorkItems": "&"
        }
    });
    /*
     * ap.worklist.worklistViewCtrl is first controller that paints the worklist.
     * It initiates query for worklist meta data
     * Paints the worklist search options
     */
    function WorklistQuoteBoxController($resource, $filter, contextPath, model, WorkListViewSrv, $log, apiGatewayService) {
        var ctrl = this;

        ctrl.model = model;

        ctrl.metaData = {};

        ctrl.worklistResource = function() {
          var httpHeaders = apiGatewayService.getDefaultHeaders();
          return $resource(contextPath + '/worklists/workitems/views/QuotesByStatus', {}, {
              query: {
                  method: 'GET',
                  isArray: false,
                  headers: httpHeaders
              },
              read: {
                  method: 'POST',
                  isArray: false,
                  headers: httpHeaders
              }
          });
        }
        //This function is called after the initial meta for worklist is retrieved
        //initView will setup any state default values for painting the UI
        ctrl.initView = function() {

            WorkListViewSrv.retrieveLookupList(ctrl.metaData.lookupLinks, {
                worklistType: 'workitems'
            });
        };

        ctrl.getItemCount = function(status) {
            for (var index in ctrl.statuses) {
                if (ctrl.statuses[index] == status) {
                    return ctrl.items[index];
                }
            }
            return 0;
        };

        ctrl.getMetaData = function(resHandler) {

            ctrl.worklistResource().query(function(data, headers) {
                apiGatewayService.pushCsrfToken(headers);
                ctrl.metaData = data.response.results.workListView;

                ctrl.initView();

                if (angular.isFunction(resHandler)) {
                    resHandler();
                }
            }, function(res){
              if(res.status === 401){
                apiGatewayService.handleAuthError();
              }
            });

        };

        ctrl.getMetaData(function() {
            ctrl.getQuoteGroups();
        });


        ctrl.lookupValue = function(key, code) {
            return WorkListViewSrv.lookupValue(key, code);
        };

        ctrl.getQuoteGroups = function() {

            ctrl.statuses = [];
            ctrl.items = [];
            ctrl.params = [];
            var keepGoing = true;
            angular.forEach(ctrl.metaData.queryInfo.queryFields, function(queryField) {
                if (queryField.dataType == 'STATUS' && keepGoing) {
                    keepGoing = false;
                    ctrl.statuses = angular.copy(queryField.operands);
                    for (var index in ctrl.statuses) {
                        var status = ctrl.statuses[index];
                        queryField.operands = [status];
                        var par = ctrl.metaData;
                        ctrl.params[index] = angular.copy(par);
                    };
                }
            });

            ctrl.getQuoteCount(0);
        };

        //get count by status using $resource chain.
        ctrl.getQuoteCount = function(index) {
            if (index < ctrl.statuses.length) {
                ctrl.worklistResource().read(ctrl.params[index], function(data, headers) {
                    apiGatewayService.pushCsrfToken(headers);
                    ctrl.items[index] = data.response.results.hits;
                    ctrl.getQuoteCount(index + 1);
                }, function(res){
                  if(res.status === 401){
                    apiGatewayService.handleAuthError();
                  }
                });
            } else {
                ctrl.params = [];
            }
        };

        ctrl.toggleStatusFilter = function(status){

          var filterOption = ctrl.getRelatedFilter(status);
          if(filterOption){
            filterOption.selected = !filterOption.selected;
            ctrl.fetchWorkItems();
          }
        }

        ctrl.getRelatedFilter = function(status){
          var relatedFilter = null;
          angular.forEach(ctrl.model.metaData.filters, function(filter) {
              if(filter.type === 'STATUS'){
                angular.forEach(filter.filterOptions, function(filterOption) {
                  if(filterOption.value === status){
                    relatedFilter = filterOption;
                  }
                });
              }
          });
          return relatedFilter;
        }

        ctrl.isSelected = function(status){
          var filterOption = ctrl.getRelatedFilter(status);
          if(filterOption){
            return filterOption.selected === true;
          } else {
            return false;
          }
        }

    }

})();
