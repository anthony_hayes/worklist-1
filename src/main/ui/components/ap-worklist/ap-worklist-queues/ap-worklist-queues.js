(function() {

    'use strict';

    angular.module('worklist')

    .component('apWorklistQueues', {
        controller: ['$resource', '$filter', 'contextPath', 'WorkListViewSrv', '$log', 'apiGatewayService', WorklistQueueController],
        template: require('raw!./ap-worklist-queues.html')
    });
    /*
     * ap.worklist.worklistViewCtrl is first controller that paints the worklist.
     * It initiates query for worklist meta data
     * Paints the worklist search options
     */
    function WorklistQueueController($resource, $filter, contextPath, WorkListViewSrv, $log, apiGatewayService) {
        var self = this;
        self.agentqueue = {};
        self.uwqueue = {};

        self.agentqueue.metaData = {};
        self.agentqueue.workItems = {};
        self.agentqueue.fetchWorkItem = true;

        self.uwqueue.metaData = {};
        self.uwqueue.workItems = {};
        self.uwqueue.fetchWorkItem = true;

        self.agentqueue.worklistResource = function(){
          var httpHeaders = apiGatewayService.getDefaultHeaders();
          return $resource(contextPath + '/worklists/workitems/views/WorkItemsRecentAgentQueue', {}, {
                query: {
                    method: 'GET',
                    isArray: false,
                    headers: {
                        Accept: 'application/json'
                    }
                },
                read: {
                    method: 'POST',
                    isArray: false,
                    headers: httpHeaders
                }
            });
        }
        self.uwqueue.worklistResource = function(){
          var httpHeaders = apiGatewayService.getDefaultHeaders();
          return $resource(contextPath + '/worklists/workitems/views/WorkItemsRecentUWQueue', {}, {
                query: {
                    method: 'GET',
                    isArray: false,
                    headers: {
                        Accept: 'application/json'
                    }
                },
                read: {
                    method: 'POST',
                    isArray: false,
                    headers: httpHeaders
                }
            });
        }

        //This function is called after the initial meta for worklist is retrieved
        //initView will setup any state default values for painting the UI
        self.initView = function(queue) {
            $log.log(queue.metaData);
            WorkListViewSrv.retrieveLookupList(queue.metaData.lookupLinks, {
                worklistType: 'workitems'
            });

            if (queue.fetchWorkItem) {
                self.fetchWorkItems(queue);
            }
        };

        self.fetchWorkItems = function(queue, resHandler) {
            var par = queue.metaData;
            queue.worklistResource().read(par, function(data, headers) {
                apiGatewayService.pushCsrfToken(headers);
                queue.workItems = data.response.results.docs;
                if (angular.isFunction(resHandler)) {
                    resHandler(data.response);
                }
            }, function(res){
              if(res.status === 401){
                apiGatewayService.handleAuthError();
              }
            });
        };

        self.getMetaData = function(queue, resHandler) {

            queue.worklistResource().query(function(data, headers) {
                apiGatewayService.pushCsrfToken(headers);
                queue.metaData = data.response.results.workListView;

                self.initView(queue);

                if (angular.isFunction(resHandler)) {
                    resHandler();
                }
            }, function(res){
              if(res.status === 401){
                apiGatewayService.handleAuthError();
              }
            });

        };

        self.getMetaData(self.agentqueue, function() {
            self.getMetaData(self.uwqueue);
        });


        self.lookupValue = function(key, code) {
            return WorkListViewSrv.lookupValue(key, code);
        };

        self.getDaysFromToday = function(lastUpdated) {
            var nowTime = (new Date()).getTime();
            var index = lastUpdated.indexOf(' ');
            var pattern = /(\d{2})\-(\d{2})\-(\d{4})/;
            var dt = lastUpdated.substring(0, index).replace(pattern, '$3/$1/$2');

            lastUpdated = dt + lastUpdated.substring(index);

            var lastDatetime = (new Date(lastUpdated)).getTime();
            var diff = nowTime - lastDatetime;
            var secs = Math.abs(diff) / 1000;
            return Math.round(secs / (60 * 60 * 24));
        };

        self.getWorkItemValue = function(field, workitem) {
            var val = workitem[field.id];
            if (field.id == 'last_update_time') {
                val = self.getDaysFromToday(workitem.last_update_time);
            } else {
                var newVal = self.lookupValue(field.id, val); //lookup using field id
                if (val == newVal) {
                    val = self.lookupValue(field.type, val); //lookup using field type
                } else {
                    val = newVal;
                }
                if (field.format) {
                    try {
                        var filtersToApply = [];
                        if (field.format.indexOf('|')) {
                            filtersToApply = field.format.split('|');
                        } else {
                            filtersToApply = field.format;
                        }

                        for (var ix = 0; ix < filtersToApply.length; ix++) {
                            var formatVal = filtersToApply[ix];
                            if (formatVal.indexOf(': ') > -1) { //multiple arguments to the filter. e.g. current : '$'
                                var tokens = formatVal.split(': ');
                                for (var i = 0; i < tokens.length; i++) { //remove any extra ' character
                                    tokens[i] = tokens[i].replace(/'/g, "");
                                }
                                val = $filter(tokens[0])(val, tokens.slice(1));
                            } else {
                                val = $filter(formatVal)(val);
                            }
                        }
                    } catch (error) {
                        console.log('Unsupported format error: ' + error);
                    }
                }
                return val;
            }
            return val;

        };
    }

})();
