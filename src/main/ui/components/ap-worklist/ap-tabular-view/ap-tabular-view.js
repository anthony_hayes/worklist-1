module.exports = (function() {

  'use strict';

  angular.module('worklist')

  .component('apTabularView', {
    controller: 'ap.worklist.worklistDataCtrl',
    template: require('raw!./ap-tabular-view.html'),
    require: {
			apWorklist:"^apWorklist"
		}
  })

})();
