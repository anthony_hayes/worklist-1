(function() {

    'use strict';

    angular.module('worklist')

    .component('apFilterPanel', {
        controller: ['model', 'WorkListViewSrv', FilterPanelController],
        template: require('raw!./ap-filter-panel.html'),
        bindings: {
          "filterPanel": "="
        }
    });
    /*
     * paints the popout worklist filter panel
     */
    function FilterPanelController(model, WorkListViewSrv) {
        var $ctrl = this;

        /* called when the viewmodel metadata changes */
        model.watch(function(){
          $ctrl.filters = angular.copy(model.metaData.filters);
          angular.forEach($ctrl.filters, function(filter){
            filter.status = {
              isopen: true
            }
          })
        })

        $ctrl.countSelected = function(filter){
          var count = 0;
          angular.forEach(filter.options, function(option){
            if(option.selected === true){
              count++;
            }
          })
          return count;
        }
        $ctrl.closeFilterPanel = function() {
          $ctrl.filterPanel.isopen = false;
        };

        $ctrl.apply = function(){
          angular.copy($ctrl.filters, model.metaData.filters);
          WorkListViewSrv.getWorkItems(function(){
            $ctrl.closeFilterPanel();
          })
        }

        /*
         * Clear all selections from the filter panel
         */
        $ctrl.clear = function() {
            var allFilters = $ctrl.filters;

            angular.forEach(allFilters, function(each) {
                angular.forEach(each.filterOptions, function(filterOption) {
                    filterOption.selected = false;
                });
            });

            /*
            * reset the query fields except entity name search - this might be
            * needed once we bring back advanced search aka more options
            *
            angular.forEach(self.workListModel.metaData.queryInfo.queryFields, function(each) {
                if (each.interactive == true && each.isSaveable == true) {
                    each.selected = false;
                }
            });
            */
        };
    }

})();
