/*
 * Created on Jan 28, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex.impl;

import com.agencyport.account.model.IAccount.AccountType;
import com.agencyport.api.worklist.pojo.Field;

/**
 * The SpecialDataReaderStore class stores the various special data readers necessary for the account SOLR index. 
 * @since 5.1
*/
public final class SpecialDataReaderStore {
	/**
	 * The <code>OTHER_NAME</code> is a constant for the other name field.
	 */
	private static final String OTHER_NAME = "other_name";
	/**
	 * The <code>ENTITY_NAME</code> is a constant for the entity name field. 
	 */
	private static final String ENTITY_NAME = "entity_name";
	/**
	 * The <code>SPECIAL_FIELDS</code> contains the list of field names that need special handling.
	 */
	private static final SpecialDataReader[] SPECIAL_FIELDS = {
		new SpecialDataReader(AccountType.P, ENTITY_NAME, ENTITY_NAME, OTHER_NAME),
		new SpecialDataReader(AccountType.U, "address", "address", "city", "state_prov_cd", "postal_code")
	};

	/**
	 * Prevent construction.
	 */
	private SpecialDataReaderStore() {
	}
	
	/**
	 * Dispenses the special data reader instance for the specified arguments.
	 * @param accountType is the account type.
	 * @param field is the field under consideration.
	 * @return the special data reader instance for the specified arguments. If none is found, then
	 * null is returned.
	 */
	public static SpecialDataReader getSpecialDataReader(AccountType accountType, Field field){
		for (SpecialDataReader specialField : SPECIAL_FIELDS){
			if ((accountType.equals(specialField.getAccountType()) || AccountType.U.equals(specialField.getAccountType()))  
				&& field.getName().equals(specialField.getFieldName()) ){
				return specialField;
			}  
		}
		return null;
	}

}
