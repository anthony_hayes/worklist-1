/*
 * Created on Jan 27, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex.impl;

import java.sql.Timestamp;

import com.agencyport.api.pojo.DataType;
import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.shared.APException;
import com.agencyport.utils.APDate;
import com.agencyport.workitem.model.IWorkItem;
import com.agencyport.workitem.model.IWorkItemStatus;
import com.agencyport.worklist.searchindex.IDataReader;
import com.agencyport.worklist.solr.manager.SolrManager;

/**
 * The WorkItemDataReader class provides the mechanism for for reading values from a work item or account objects 
 * for the purposes of updating a search index. 
 * @since 5.1 
 */
public class WorkItemDataReader implements IDataReader {
	/**
	 * The <code>dataSource</code> is the data source for this reader.
	 */
	private IWorkItem dataSource;
	
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public Object fetchValue(Field field) throws APException{
		Object value = dataSource.getPropertyValue(field.getWorkItemPropertyName());
		DataType dataType = field.getType(); 
		if (value != null){
			switch (dataType){
			case DATE:
				APDate date = (APDate) value;
				value = SolrManager.formatDate(date.getUtilDate());
				break;
			case TIME:
				Timestamp time = (Timestamp) value;
				value = SolrManager.formatDate(time);
				break;
			case STATUS:
				IWorkItemStatus wiStatus = (IWorkItemStatus) value;
				value = wiStatus.getMnemomic();
				break;
			case STRING:
				value = field.getEncrypted() ? Field.encryptValue(value.toString()) : value; 
				break;
			default:
				value = interpretValueForField(value);
				break;
			}
		} 
		return value;
	}
	
	/**
	 * Interprets a value for a field.
	 * @param value is the value to interpret.
	 * @return a the interpreted value.
	 */
	private Object interpretValueForField(Object value) {
		if (value instanceof Boolean){
			// We want a 1 or 0 not a true / false
			Boolean boolValue = (Boolean) value;
			return boolValue ? 1 : 0;
		} else {
			return value.toString();
		}
	}

	
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public Object read(Field field) throws APException {
		return fetchValue(field);
	}

	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public void setDataSource(Object dataSource) throws APException{
		if (dataSource instanceof IWorkItem){
			this.dataSource = (IWorkItem) dataSource;
		} else {
			throw new APException(String.format("Unacceptable data source type '%s' passed. Expected type '%s'", dataSource != null ? dataSource.getClass() : "null", IWorkItem.class.getName()));
		}
		
	}

	/**
	 * @return the dataSource
	 */
	public final IWorkItem getDataSource() {
		return dataSource;
	}

}
