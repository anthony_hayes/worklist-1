/**
 * Contains supporting infrastructure for search index integration and processing. 
 */
package com.agencyport.worklist.searchindex;