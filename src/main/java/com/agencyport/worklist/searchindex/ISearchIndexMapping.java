/*
 * Created on Jan 30, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex;

import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.worklist.view.LocalizedContent;

/**
 * The indexMap interface defines the canonical API for accessing index mapping meta data.
 * @since 5.1
 */
public interface ISearchIndexMapping extends Iterable<Field>{
	/**
	 * Gets an field instance for the given name for the current locale.
	 * @param id can be the id, name, indexFieldName or work item property attribute.
	 * @return an field instance for the given name.
	 */
	Field getField(String id);
	/**
	 * Gets the localized content for a given field.
	 * @param fieldId is the field id.
	 * @return the localized content for a given field.
	 */
	LocalizedContent getLocalizedContent(String fieldId);
}
