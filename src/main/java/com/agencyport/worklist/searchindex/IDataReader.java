/*
 * Created on Mar 10, 2016 by ldeane AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex;

import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.shared.APException;

/**
 * The IDataReader interface provides a common derivation for all the various
 * types of data reader implementations. A data reader is responsible for reading
 * a data source. 
 * @since 5.1
 */
public interface IDataReader {
	/**
	 * Sets the data source for this reader.
	 * @param dataSource is the data source.
	 * @throws APException if the process of setting the data source went wrong.
	 */
	void setDataSource(Object dataSource) throws APException;
	/**
	 * Reads the value from the data source for the specified field. This may engage special processing for field values that
	 * are derived from other fields.  
	 * @param field is the field under consideration.
	 * @return the value from the data source for the specified field. A null return
	 * value means that none was read.
	 * @throws APException if the read operation failed.
	 */
	Object read(Field field) throws APException;
	
	/**
	 * Responsible for fetching the value for the given field directly from the data source. 
	 * @param field is the field under consideration.
	 * @return the value for the given field from the data source.
	 * @throws APException if there was a severe issue trying to access the data source.
	 */
	Object fetchValue(Field field) throws APException;
}
