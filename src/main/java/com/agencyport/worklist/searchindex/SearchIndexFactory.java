/*
 * Created on Feb 2, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import com.agencyport.factory.GenericFactory;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.shared.APException;
import com.agencyport.utils.AppProperties;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.pojo.WorkListView;
import com.agencyport.worklist.searchindex.impl.XMLDataReader;
import com.agencyport.worklist.searchindex.solr.SolrIndexDataMapper;
import com.agencyport.worklist.searchindex.solr.SolrWorkListSearchProvider;

/**
 * The SearchIndexFactory class provides the factory methods for various
 * search index related operations. Applications can create their own derivations by registering their own extension under the 
 * application property <b>application.searchindex_factory_classname</b>.
 * @since 5.1
 */
public class SearchIndexFactory {
	/**
	 * The <code>DATA_READER_CLASS_NAME_RESOLVER_MAP</code> is a map of IDataReader class names by work item type.
	 */
	private static final Map<WorkItemType, String> DATA_READER_CLASS_NAME_RESOLVER_MAP = loadDataReaderClassNameMap(); 
			
	/**
	 * Initializes the map of class names by work item type that implement the IDataReader interface. 
	 * @return the map of class names by work item type that implement the IDataReader interface.
	 */
	private static Map<WorkItemType, String> loadDataReaderClassNameMap(){
		Map<WorkItemType, String> map = new HashMap<>();
		AppProperties props = AppProperties.getAppProperties();
		map.put(WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE, props.getStringProperty("workitem.data_reader_classname", XMLDataReader.class.getName()));
		map.put(WorkItemType.ACCOUNT_WORK_ITEM_TYPE, props.getStringProperty("account.data_reader_classname", XMLDataReader.class.getName()));
		return map;
	}

	/**
	 * The <code>RESULT_SET_DATA_READER_CLASS_NAME_RESOLVER_MAP</code> is a map of IDataReader class names by work item type for result set processing.
	 */
	private static final Map<WorkItemType, String> RESULT_SET_DATA_READER_CLASS_NAME_RESOLVER_MAP = loadResultSetDataReaderClassNameMap(); 
			
	/**
	 * Initializes the map of class names by work item type that implement the IDataReader interface for result set processing. 
	 * @return the map of class names by work item type that implement the IDataReader interface.
	 */
	private static Map<WorkItemType, String> loadResultSetDataReaderClassNameMap(){
		Map<WorkItemType, String> map = new HashMap<>();
		AppProperties props = AppProperties.getAppProperties();
		//map.put(WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE, props.getStringProperty("workitem.result_set_data_reader_classname", WorkItemResultSetDataReader.class.getName()));
		//map.put(WorkItemType.ACCOUNT_WORK_ITEM_TYPE, props.getStringProperty("account.result_set_data_reader_classname", AccountResultSetDataReader.class.getName()));
		return map;
	}
	
	
	/**
	 * The <code>SINGLETON</code> is the single instance of this class.
	 */
	private static final SearchIndexFactory SINGLETON = createSearchIndexFactory();
	
	/**
	 * Creates an instance of the search index factory.
	 * @return an instance of the search index factory.
	 */
	private static SearchIndexFactory createSearchIndexFactory(){
		try {
			return GenericFactory.create("application.searchindex_factory_classname", SearchIndexFactory.class.getName());
		} catch (APException apException){
			ExceptionLogger.log(apException, SearchIndexFactory.class, "createIndex");
			return new SearchIndexFactory();
		}
	}

	/**
	 * Returns the single instance of this class.
	 * @return the single instance of this class.
	 */
	public static SearchIndexFactory get(){
		return SINGLETON;
	}
	/**
	 * Constructs an instance.
	 */
	public SearchIndexFactory() {
	}
	
	/**
	 * Creates a work list search provider
	 * @param returnMediaType is the return media type
	 * @param workItemType is work item type.
	 * @param solrWorkListView is the SOLR worklist view instance.
	 * @param securityProfile is the security profile of the current user.
	 * @return a work list search provider
	 * @throws APException if the initialization failed on the worklist search provider instance.
	 * @since 5.1
	 */
	public IWorkListSearchProvider createWorkListSearchProvider(MediaType returnMediaType, WorkItemType workItemType, WorkListView solrWorkListView, ISecurityProfile securityProfile) throws APException {
		IWorkListSearchProvider workListSearchProvider = GenericFactory.create("application.work_list_search_provider_classname", SolrWorkListSearchProvider.class.getName());
		if (workListSearchProvider != null){
			workListSearchProvider.initialize(returnMediaType, workItemType, solrWorkListView, securityProfile);
		}
		return workListSearchProvider;
	}
	
	
	/**
	 * Creates a data reader instance for the given work item type. The reader is designed
	 * to read a derivation of the IWorkItem interface.
	 * @param workItemType is the work item type.
	 * @return a data reader instance for the given work item type. 
	 * @throws APException if the instantiation of the data reader instance failed.
	 */
	public IDataReader createDataReader(WorkItemType workItemType) throws APException {
		return GenericFactory.create(DATA_READER_CLASS_NAME_RESOLVER_MAP.get(workItemType));
	}

	/**
	 * Creates a data reader instance for the given work item type. The reader is designed
	 * to read a SQL result set.
	 * @param workItemType is the work item type.
	 * @return a data reader instance for the given work item type. 
	 * @throws APException if the instantiation of the data reader instance failed.
	 */
	public IDataReader createResultSetDataReader(WorkItemType workItemType) throws APException {
		return GenericFactory.create(RESULT_SET_DATA_READER_CLASS_NAME_RESOLVER_MAP.get(workItemType));
	}

	/**
	 * Creates an index data mapper instance.
	 * @param index is the name of the index.
	 * @param dataReader is the data reader.
	 * @return an index data mapper instance. 
	 * @throws APException if the instantiation of the data mapper failed.
	 */
	public IndexDataMapper createIndexDataMapper(String index, IDataReader dataReader) throws APException {
		return new SolrIndexDataMapper(index, dataReader);
	}
}
