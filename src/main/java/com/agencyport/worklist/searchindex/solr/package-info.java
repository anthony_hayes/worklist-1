/**
 * Contains supporting infrastructure for search index integration and processing specific
 * to SOLR index integration.
 */
package com.agencyport.worklist.searchindex.solr;