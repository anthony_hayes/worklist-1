/*
 * Created on Feb 2, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.searchindex.solr;

import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.agencyport.worklist.searchindex.IQueryResults;

/**
 * The SolrQueryResults class provides an implementation of the query results interface
 * specific to SOLR index processing.
 * @since 5.1
 */
public class SolrQueryResults implements IQueryResults {
	/**
	 * The <code>solrDocumentList</code> is the SOLR document list.
	 */
	private final SolrDocumentList solrDocumentList;
	
	/**
	 * The SolrQueryResultRecord class provides an implementation of the query result record interface
	 * for SOLR index processing.
	 */
	private static final class SolrQueryResultRecord implements IQueryResultRecord {
		/**
		 * The <code>solrDocument</code> is the SOLR document.
		 */
		private final SolrDocument solrDocument;
		/**
		 * Creates an instance.
		 * @param solrDocument see {@link #solrDocument}
		 */
		private SolrQueryResultRecord(SolrDocument solrDocument){
			this.solrDocument = solrDocument;
		}
		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public Iterator<Entry<String, Object>> iterator() {
			return solrDocument.iterator();
		}
		
	}
	/**
	 * Constructs an instance.
	 * @param solrDocumentList is the SOLR document list.
	 */
	public SolrQueryResults(SolrDocumentList solrDocumentList) {
		this.solrDocumentList = solrDocumentList;
	}


	/** 
	 * {@inheritDoc}
	 */

	@Override
	public long getNumberOfHits() {
		return solrDocumentList.getNumFound();
	}

	/** 
	 * {@inheritDoc}
	 */

	@Override
	public long getStartingRecordNumber() {
		return solrDocumentList.getStart();
	}

	/** 
	 * {@inheritDoc}
	 */

	@Override
	public int getNumberOfResults() {
		return solrDocumentList.size();
	}


	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public IQueryResultRecord getQueryResult(int index) {
		SolrDocument solrDocument = solrDocumentList.get(index);		
		return new SolrQueryResultRecord(solrDocument);
	}
	
	

}
