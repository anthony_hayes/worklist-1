/**
 * 
 */
package com.agencyport.worklist.spring;

import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jndi.JndiObjectFactoryBean;

import com.agencyport.worklist.spring.HealthMonitor;
import com.agencyport.worklist.spring.SpringContextBridge;
import com.agencyport.bootservice.BootServiceDriver;
import com.agencyport.logging.LoggingManager;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.repository.RedisRepository;
import com.agencyport.security.repository.SecurityProfileRepository;
import com.agencyport.shutdown.ApplicationShutdownOfficer;

/**
 * 
 * The Spring AppConfig class
 */
@Configuration
public class AppConfig {
	
	/**
	 * The <code>LOGGER</code> logger for the app config
	 */
	private static final Logger LOGGER = LoggingManager.getLogger(AppConfig.class.getName());
	
	
	@Autowired
	private Environment environment;
	
	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		String jndiDatasourceName = environment.getProperty("datasource");
		LOGGER.info("Configuring dataSource bean for: " + jndiDatasourceName);
		JndiObjectFactoryBean dataSource = new JndiObjectFactoryBean();
		dataSource.setJndiName(jndiDatasourceName);
		try {
			dataSource.afterPropertiesSet();
		} catch (IllegalArgumentException | NamingException e) {
			throw new RuntimeException(e);
		}
		return (DataSource) dataSource.getObject();
	}
	
	@Bean(name = "bootServiceDriver")
	public Class<BootServiceDriver> bootServiceDriver(DataSource dataSource) {
		
		LOGGER.info("Initializing BootService..");
			BootServiceDriver.bootSubSystems();
			boolean success = BootServiceDriver.getSystemBootSuccessFlag();
			if (!success) {
				String[] problematicBootstrapServices = BootServiceDriver.getProblematicBootstrapServices();
				for (int ix = 0; ix < problematicBootstrapServices.length; ix++) {
					LOGGER.severe("Problem encountered with bootstrap service: '" + problematicBootstrapServices[ix] + "'");
				}
			} else {
				LOGGER.info("All bootstrap services started normally");
			}
			return BootServiceDriver.class;
		
		}
	
	@Bean(name="disposeTestResource")
    public DisposableBean disposeTestResource() {
    	return new DisposableBean() {
			@Override
			public void destroy() throws Exception {
				ApplicationShutdownOfficer.getApplicationShutdownOfficer().shutdown();
				
			}
		};
    	
    }
	
	 /**
     * Spring bean for the SpringContextBridge
     * SpringContextBridge is the bridge to the "non managed" world.
     * @return SpringContextBridge object
     */
    @Bean
    SpringContextBridge getSpringContextBridge() {
        return new SpringContextBridge();
    }


    /**
     * Spring bean for the Redis Repository
     * @return SecurityProfileRepository
     */
    @Bean
    public RedisRepository<String, ISecurityProfile> createRedisRepository() {
        return new SecurityProfileRepository();
    }
    
	/**
     * Spring bean for the health monitor class
     * @return an instance of health monitor
     */
    @Bean
    HealthMonitor getHealthMonitor(DataSource dataSource) {
        return new HealthMonitor(dataSource);
    }
}
