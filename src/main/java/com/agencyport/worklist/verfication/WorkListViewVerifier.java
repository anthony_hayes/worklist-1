/*
 * Created on Jan 8, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.verfication;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.agencyport.api.BaseResource;
import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.domXML.widgets.LOBCode;
import com.agencyport.fieldvalidation.validators.builtin.NumericValidator;
import com.agencyport.rest.RestValidationException;
import com.agencyport.security.exception.SecurityException;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.shared.APException;
import com.agencyport.utils.APDate;
import com.agencyport.workitem.model.IWorkItemStatusManager;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.factory.WorklistFactory;
import com.agencyport.worklist.pojo.Filter;
import com.agencyport.worklist.pojo.ListView;
import com.agencyport.worklist.pojo.QueryField;
import com.agencyport.worklist.pojo.QueryInfo;
import com.agencyport.worklist.pojo.SavedSearch;
import com.agencyport.worklist.pojo.SavedSearchInfo;
import com.agencyport.worklist.pojo.SortInfo;
import com.agencyport.worklist.pojo.SortOption;
import com.agencyport.worklist.pojo.WorkListView;
import com.agencyport.worklist.searchindex.ISearchIndexMapping;
import com.agencyport.worklist.view.CompiledWorkListView;
import com.agencyport.worklist.view.IWorkListViewProvider;
import com.agencyport.worklist.view.WorkListViewResourceProvider;

/**
 * The WorkListViewVerifier class validates SolrWorkListView instances.
 * @since 5.1 
 */
@SuppressWarnings("unchecked")
public class WorkListViewVerifier extends WorkListViewNameVerifier {
	/**
	 * The <code>GET_RELATED_FIELD_ID</code> a constant for a frequently used string.
	 */
	private static final String GET_RELATED_FIELD_ID = "getRelatedFieldId";
	/**
	 * The <code>RELATED_FIELD_ID</code> a constant for a frequently used string.
	 */
	private static final String RELATED_FIELD_ID = "relatedFieldId";
	/**
	 * The <code>GET_FORMAT</code> a constant for a frequently used string.
	 */
	private static final String GET_FORMAT = "getFormat";
	/**
	 * The <code>FORMAT</code> a constant for a frequently used string.
	 */
	private static final String FORMAT = "format";
	/**
	 * The <code>INVALID_VALUE_FOUND_FOR_FIELD_NAMED</code> contains an error message template the is used many times.
	 */
	private static final String INVALID_VALUE_FOUND_FOR_FIELD_NAMED = "Invalid %s value: '%s' found for field named: '%s'";
	/**
	 * The <code>workListView</code> is the incoming instance which is being validated.
	 */
	private final WorkListView workListView;
	/**
	 * The <code>configuredWorkListView</code> is the reference good instance which the incoming instance will be
	 * compared against.
	 */
	private final WorkListView configuredWorkListView;
	/**
	 * The <code>workItemStatusManager</code> is used to validate work item status values.
	 */
	private final IWorkItemStatusManager workItemStatusManager = WorklistFactory.createWorkItemStatusManager();
	
	/**
	 * The <code>indexMap</code> is the index mapping resource.
	 */
	private final ISearchIndexMapping indexMap;
	/**
	 * Constructs an instance.
	 * @param workListView is the worklist view to validate.
	 * @param viewResource is the CompiledWorkListView from the configuration service
	 * @param workItemType is the work item type.
	 * @param securityProfile is the security profile of the current user.
	 * @throws APException propagated from {@link WorkListViewResourceProvider#getWorkListViewResource(String)} or
	 * from {@link IWorkListViewProvider#getView(CompiledWorkListView, WorkItemType, String, int, ISecurityProfile, boolean)}
	 */
	public WorkListViewVerifier(WorkListView workListView, CompiledWorkListView viewResource, WorkItemType workItemType, ISecurityProfile securityProfile) throws APException {
		super(workListView.getName(), viewResource);
		this.workListView = workListView;
		if (this.workListViewResource != null){
			IWorkListViewProvider viewProvider = WorklistFactory.createWorkListViewProvider();
			this.configuredWorkListView = viewProvider.getView(viewResource, workItemType, workListView.getName(), SavedSearch.getDefaultSavedSearchId(), securityProfile, false);
			this.indexMap = this.workListViewResource.getIndexMap(); 
		} else {
			this.configuredWorkListView = null;
			this.indexMap = null; 
		}
	}
	
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public void validateParameters(BaseResource baseResource) throws RestValidationException {
		super.validateParameters(baseResource);
		validateFilters();
		validateSortInfo();
		validateQueryInfo();
		validateListViews();
		validateSavedSearches();
	}
	
	
	/**
	 * Validates the filters on the incoming work list view against those on the configured, known good work list view.
	 * @throws RestValidationException if the validation fails.
	 */
	protected void validateFilters() throws RestValidationException {
		List<Filter> incomingFilters = workListView.getFilters();
		List<Filter> basisFilters = configuredWorkListView.getFilters();
		if (incomingFilters == null || basisFilters == null){
			// nothing to validate
			return;	
		}
		Map<String, Filter> basisFilterMap = (Map<String, Filter>) validateKeyedItems(incomingFilters, basisFilters);
		for (Filter incomingFilter :  incomingFilters){
			Filter basisFilter = basisFilterMap.get(incomingFilter.getKey());
			validateProperty(FORMAT, GET_FORMAT, incomingFilter, basisFilter);
			validateProperty(FORMAT, GET_FORMAT, incomingFilter, basisFilter);
			validateProperty(RELATED_FIELD_ID, GET_RELATED_FIELD_ID, incomingFilter, basisFilter);
			validateProperty("type", "getType", incomingFilter, basisFilter);
			validateKeyedItems(incomingFilter.getFilterOptions(), basisFilter.getFilterOptions());
		}
	}
	
	/**
	 * Validates that the incoming sort information instance compares with the configured, known good sort information instance.
	 * @throws RestValidationException if the validation fails.
	 */
	protected void validateSortInfo() throws RestValidationException {
		SortInfo incomingSortInfo = workListView.getSortInfo();
		SortInfo basisSortInfo = configuredWorkListView.getSortInfo();
		if (incomingSortInfo == null || basisSortInfo == null){
			// nothing to validate
			return;
		}
		validateKeyedItem(incomingSortInfo, basisSortInfo);
		Map<String, SortOption> basisSortOptions = (Map<String, SortOption>) validateKeyedItems(incomingSortInfo.getSortOptions(), basisSortInfo.getSortOptions());
		for (SortOption incomingSortOption : incomingSortInfo.getSortOptions()){
			SortOption basisSortOption = basisSortOptions.get(incomingSortOption.getKey());
			validateProperty(RELATED_FIELD_ID, GET_RELATED_FIELD_ID, incomingSortOption, basisSortOption);
		}
	}
	
	/**
	 * Validates that the incoming query information instance compares with the configured, known good query information instance.
	 * @throws RestValidationException if the validation fails.
	 */
	protected void validateQueryInfo() throws RestValidationException {
		QueryInfo incomingQueryInfo = workListView.getQueryInfo();
		QueryInfo basisQueryInfo = configuredWorkListView.getQueryInfo();
		if (incomingQueryInfo == null || basisQueryInfo == null){
			return;
		}
		validateKeyedItem(incomingQueryInfo, basisQueryInfo);
		validateProperty("transactionId", "getTransactionId", incomingQueryInfo, basisQueryInfo);
		validateProperty("pageId", "getPageId", incomingQueryInfo, basisQueryInfo);
		Map<String, QueryField> basisQueryFields = (Map<String, QueryField>) validateKeyedItems(incomingQueryInfo.getQueryFields(), basisQueryInfo.getQueryFields());
		for (QueryField incomingQueryField : incomingQueryInfo.getQueryFields()){
			QueryField basisQueryField = basisQueryFields.get(incomingQueryField.getKey());
			validateProperty(RELATED_FIELD_ID, GET_RELATED_FIELD_ID, incomingQueryField, basisQueryField);
			validateProperty("dataType", "getDataType", incomingQueryField, basisQueryField);
		}
		validateOperandsOnQueryFields();
	}
	
	/**
	 * Validates that the incoming list views compares with the configured, known good list views.
	 * @throws RestValidationException if the validation fails.
	 */
	protected void validateListViews() throws RestValidationException {
		List<ListView> incomingListViews = workListView.getListViews();
		if (incomingListViews == null){
			return;
		}
		for (ListView incomingListView : incomingListViews){
			for (Field incomingField : incomingListView.getFields()){
				try {
					// The following getField method throws a runtime exception if the id value is bogus.
					ListView view = configuredWorkListView.getListView(incomingListView.getType());
					Field basisField = view.getField(incomingField.getId());
					validateKeyedItem(incomingField, basisField);
					validateProperty(FORMAT, GET_FORMAT, incomingField, basisField);
					validateProperty("type", "getType", incomingField, basisField);
					validateProperty("indexFieldName", "getIndexFieldName", incomingField, basisField);
				} catch (RuntimeException noFieldException){
					throw new RestValidationException(noFieldException);
				}
			}
			
		}
	}
	
	/**
	 * Validates the current search name and saved search names.
	 * @throws RestValidationException if the validation fails.
	 */
	protected void validateSavedSearches() throws RestValidationException {
		SavedSearchInfo incomingSavedSearchInfo = workListView.getSavedSearchInfo();
		validateSuspiciousContent(incomingSavedSearchInfo.getCurrentSearch().getName(), "Current search name");
		for (SavedSearch savedSearch : incomingSavedSearchInfo.getSavedSearches()){
			validateSuspiciousContent(savedSearch.getName(), "Saved search name");
		}
	}
	
	/**
	 * Validates the operand values on the query fields.
	 * @throws RestValidationException on the first invalid operand.
	 */
	protected void validateOperandsOnQueryFields() throws RestValidationException {
		QueryInfo basisQueryInfo = configuredWorkListView.getQueryInfo();
		for (QueryField queryField : workListView.getQueryInfo().getQueryFields()){
			Field relatedField = indexMap.getField(queryField.getRelatedFieldId());
			
			if(!queryField.isInteractive()){
				if(!basisQueryInfo.getQueryField(queryField.getRelatedFieldId()).equals(queryField)){
					throw new RestValidationException(String.format(INVALID_VALUE_FOUND_FOR_FIELD_NAMED, relatedField.getType().name(), queryField.toString(), relatedField.getKey()));
				}
			}else{
				for (String operand : queryField.getOperands()){
					if(!queryField.getSelected()){
						continue;
					}
						
					switch(relatedField.getType()){
					
					case INTEGER:	
					case DECIMAL:	
						validateNumber(operand, relatedField);
						break;
						
					case DATE:
						validateDate(operand, relatedField);
						break;
						
					case STATUS:
						validateStatus(operand, relatedField);
						break;
	
					case LOB:
						validateLOB(operand, relatedField);
						break;
	
					case TIME:
						validateDate(operand, relatedField);
						break;
					case STRING:
					default:	
						validateString(operand, relatedField);
						break;
						
					}
					
				}
			}
		}
	}
	
	/**
	 * Validates the value is a numeric.
	 * @param value is the value to validate.
	 * @param relatedField is its related field definition.
	 * @throws RestValidationException if the value does not validate.
	 */
	protected void validateNumber(String value, Field relatedField) throws RestValidationException {
		if (!NumericValidator.isNumeric(value)){
			throw new RestValidationException(String.format(INVALID_VALUE_FOUND_FOR_FIELD_NAMED, relatedField.getType().name(), value, relatedField.getKey()));
		}
	}
	
	/**
	 * Validates the value is a date.
	 * @param value is the value to validate.
	 * @param relatedField is its related field definition.
	 * @return a date instance if valid.
	 * @throws RestValidationException if the value does not validate.
	 */
	protected APDate validateDate(String value, Field relatedField) throws RestValidationException {
		try {
			return new APDate(value);
		} catch (ParseException parseException){
			throw new RestValidationException(String.format(INVALID_VALUE_FOUND_FOR_FIELD_NAMED, relatedField.getType().name(), value, relatedField.getKey()), parseException);
		}
	}
	
	/**
	 * Validates the value is a valid status mnemonic.
	 * @param value is the value to validate.
	 * @param relatedField is its related field definition.
	 * @throws RestValidationException if the value does not validate.
	 */
	protected void validateStatus(String value, Field relatedField) throws RestValidationException {
		try {
			workItemStatusManager.get(value);
		} catch (RuntimeException noStatusException){
			throw new RestValidationException(String.format(INVALID_VALUE_FOUND_FOR_FIELD_NAMED, relatedField.getType().name(), value, relatedField.getKey()), noStatusException);
		}
	}
	
	/**
	 * Validates the value is a known LOB.
	 * @param value is the value to validate.
	 * @param relatedField is its related field definition.
	 * @throws RestValidationException if the value does not validate.
	 */
	protected void validateLOB(String value, Field relatedField) throws RestValidationException {
		if (LOBCode.getRecognizedLobCodes().get(value) == null){
			throw new RestValidationException(String.format(INVALID_VALUE_FOUND_FOR_FIELD_NAMED, relatedField.getType().name(), value, relatedField.getKey()));
		}
	}
	
	/**
	 * Validates the value does not have any XSS sequences.
	 * @param value is the value to validate.
	 * @param relatedField is its related field definition.
	 * @throws RestValidationException if the value does not validate.
	 */
	protected void validateString(String value, Field relatedField) throws RestValidationException {
		validateSuspiciousContent(value, relatedField.getId());
	}
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public void checkAuthorization(BaseResource baseResource, ISecurityProfile securityProfile) throws SecurityException {
		// no authorization needed 
	}
	
	
}
