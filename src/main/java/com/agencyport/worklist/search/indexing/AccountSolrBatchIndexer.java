/*
 * Created on Apr 29, 2014 by dan AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.search.indexing;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.agencyport.database.DatabaseAgent;
import com.agencyport.database.provider.DatabaseResourceAgent;
import com.agencyport.workitem.model.IWorkItemStatus;
import com.agencyport.workitem.model.IWorkItemStatusManager;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.factory.WorklistFactory;
import com.agencyport.worklist.searchindex.IndexNames;

/**
 * The AccountSolrBatchIndexer class is responsible for batch indexing of the account index.
 */
public class AccountSolrBatchIndexer extends AbstractSolrBatchIndexer {
	
	/**
	 * The <code>SELECT_ACCOUNT</code> is the select query for gathering accounts. It gathers all accounts except for those that are inactive (where status not = 120).
	 */
	private static final String SELECT_ACCOUNT = DatabaseAgent.databaseNormalizeSQLStatement("select * from ${db_table_prefix}account where status <> ?");
	
	/**
	 * Constructs an instance.
	 */
	public AccountSolrBatchIndexer(){
		super(IndexNames.ACCOUNT_SOLR_INDEX_NAME, WorkItemType.ACCOUNT_WORK_ITEM_TYPE);
	}

	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	protected PreparedStatement getPreparedStatement(DatabaseResourceAgent dra) throws SQLException {
		PreparedStatement  pstmt = dra.getPreparedStatement(SELECT_ACCOUNT);
		IWorkItemStatusManager statusManager = WorklistFactory.createWorkItemStatusManager();
		IWorkItemStatus accountInactiveStatus = statusManager.get(IWorkItemStatus.INACTIVE);
		pstmt.setInt(1,  accountInactiveStatus.getStatusCodeValue().intValue());
		return pstmt;
	}
	
}
