package com.agencyport.worklist.search.indexing;

import com.agencyport.shared.APException;

/**
 * Interface for classes that perform batch updates to a Solr index.
 */
public interface ISolrBatchIndexer {
	/**
	 * Generates all documents for the applicable index collection and provides them to the {@link ISolrDocumentIndexer}
	 * who should make sure they get added to the index
	 * @param indexer is a reference to the SOLR document indexer.
	 * @throws APException if something went wrong when reading documents for the index 
	 */
	void provideDocumentsToIndexer(ISolrDocumentIndexer indexer) throws APException;
}
