package com.agencyport.worklist.solr.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.agencyport.account.AccountManagementFeature;
import com.agencyport.database.provider.DatabaseResourceAgent;
import com.agencyport.domXML.widgets.LOBCode;
import com.agencyport.factory.GenericFactory;
import com.agencyport.id.Id;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;
import com.agencyport.pool.IPoolableObjectFactory;
import com.agencyport.pool.ObjectPool;
import com.agencyport.security.exception.SecurityException;
import com.agencyport.security.model.IUserGroups;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.security.profile.impl.SecurityProfileManager;
import com.agencyport.shared.APException;
import com.agencyport.shutdown.ApplicationShutdownOfficer;
import com.agencyport.shutdown.IResourceDestroyer;
import com.agencyport.utils.AppProperties;
import com.agencyport.utils.text.StringUtilities;
import com.agencyport.workitem.factory.WorkItemFactory;
import com.agencyport.workitem.impl.WorkItemOpenMode;
import com.agencyport.workitem.model.IWorkItem;
import com.agencyport.workitem.model.IWorkItemActionResolver;
import com.agencyport.workitem.model.IWorkItemManager;
import com.agencyport.workitem.model.IWorkItemStatus;
import com.agencyport.workitem.model.IWorkItemStatusManager;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.factory.WorklistFactory;

/**
 * The SolrManager class handles the interactions with the SOLR server. It negotiates all queries and updates with the
 * SOLR server.
 * @since 5.0
 */
public class SolrManager implements ISolrManager, IResourceDestroyer {
	/**
	 * The <code>LOGGER</code> for SolrManager.
	 */
	private static final Logger LOGGER = LoggingManager.getLogger(SolrManager.class.getPackage().getName());
	
	/**
	 * The name of the request header that stores the Solr token.
	 */
	public static final String SOLR_SECURITY_TOKEN = "X-Agencyport-Token";
	
	/**
	 * The name of the request header that stores the Solr token type.
	 */
	public static final String SOLR_SECURITY_TOKEN_TYPE = "X-Agencyport-Token-Type";
	
	/**
	 * Constant for UTC Date formatter that is compatible with Solr date type.
	 */
	private static final String UTC_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	
	/**
	 * The parameter key for the logon id for requests sent to the SolrSecurityServlet.
	 */
	public static final String LOGON_ID_PARAM = "logon_id";
	
	/**
	 * The parameter key for the Solr XML for POST requests sent to the SolrSecurityServlet.
	 */
	public static final String XML_PARAM = "xml";
	
	/**
	 * The parameter key for the locale string for requests sent to SolrFilter from SolrSecurityServlet.
	 */
	public static final String LOCALE_PARAM = "locale";
	
	/**
	 * Constant for the index_type URL parameter name.
	 */
	public static final String INDEX_TYPE_PARAM = "index_type";
	
	/**
	 * Constant for the worklist index.
	 */
	public static final String INDEX_TYPE_WORKLIST = "worklist";
	
	/**
	 * Constant for the account index.
	 */
	public static final String INDEX_TYPE_ACCOUNT = "account";
	
	/**
	 * The constant for the responseHeader key in the JSON Solr response. 
	 */
	public static final String SOLR_RESPONSE_HEADER = "responseHeader";
	
	/**
	 * The constant for the status key in the JSON Solr response.
	 */
	public static final String SOLR_STATUS = "status";
	
	/**
	 * Success status value for Solr JSON responses.
	 */
	public static final int SOLR_SUCCESS_STATUS = 0;
	
	/**
	 * Error status value for Solr JSON responses.
	 */
	public static final int SOLR_ERROR_STATUS = 1;
	
	/**
	 * The error description of a Solr JSON response.
	 */
	public static final String SOLR_ERROR_DESCRIPTION = "description";
	
	/**
	 * Status code for successful HTTP request.
	 */
	private static final int HTTP_SUCCESS = HttpServletResponse.SC_OK;
	
	/**
	 * Singleton instance of the ISolrManager to be used in the application.
	 */
	private static final ISolrManager SOLR_MANAGER = createSolrManager();
	
	/**
	 * The <code>DEFAULT_OBJECT_POOL_MAX_SIZE</code> is the maximum size our solr server object pool can grow up to.
	 */
	private static final int DEFAULT_OBJECT_POOL_MAX_SIZE  = 10;

	/**
	 * The <code>workItemStatusManager</code> is provided for general use to any subclass.
	 */
	private final IWorkItemStatusManager workItemStatusManager = WorklistFactory.createWorkItemStatusManager();
	/**
	 * The <code>solrServerObjectPool</code> is the object pool of SOLR server instances.
	 */
	private final ObjectPool<String, HttpSolrServer> solrServerObjectPool = createHttpSolrServerObjectPool();
	
	/**
	 * The <code>urlResolver</code> used to map index types to urls.
	 */
	private final ISolrUrlResolver urlResolver = SolrUrlResolver.get();
	/**
	 * The SolrServerFactory class dispenses out new HttpSolrServer instances in compliance with the object
	 * pool infrastructure.
	 */
	private static final class SolrServerFactory implements IPoolableObjectFactory<String, HttpSolrServer> {

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public HttpSolrServer createNewPoolableObject(String poolKey) throws APException {
			return createSolrServer();
		}
		
	}
	/**
	 * The <code>solrServerFactory</code> is the implementation of the poolable object factory.
	 */
	private final IPoolableObjectFactory<String, HttpSolrServer> solrServerFactory  = new SolrServerFactory();
	
	
	/**
	 * Creation method for the ISolrManager instance.
	 * @return	An instance of ISolrManager.
	 */
	private static ISolrManager createSolrManager() {
		ISolrManager manager = null;
		try {
			manager = GenericFactory.create("solr_manager", SolrManager.class.getName());
		}
		catch(APException e) {
			ExceptionLogger.log(e, SolrManager.class, "createSolrManager");
			throw new IllegalStateException("Unable to instantiate instance of SolrManager.", e);
		}
		
		return manager;
	}
	
	/**
	 * Singleton getter for SolrManager instance.
	 * @return	The singleton instance of ISolrManager.
	 */
	public static ISolrManager getSolrManager() {
		return SOLR_MANAGER;
	}
	
	/**
	 * Constructs an instance.
	 */
	public SolrManager(){
		ApplicationShutdownOfficer.getApplicationShutdownOfficer().register(this);
	}
	
	/**
	 * Creates an object pool to hold HTTP SOLR server instances.
	 * @return an object pool to hold HTTP SOLR server instances.
	 */
	private static ObjectPool<String, HttpSolrServer> createHttpSolrServerObjectPool(){
		int poolSize = AppProperties.getAppProperties().getIntProperty(SolrManager.class.getName() + ".max_pool_size", DEFAULT_OBJECT_POOL_MAX_SIZE);
		return new ObjectPool<>(SolrManager.class.getName() + ".solr_server_pool", poolSize);
	}
	
	/**
	 * Creation method for the SolrServer instance.
	 * 
	 * @return		An instance of HttpSolrServer.
	 */
	private static HttpSolrServer createSolrServer() {
		String url = AppProperties.getAppProperties().getStringProperty("my_portal_app");
		HttpSolrServer server = new HttpSolrServer(url);
		
		return server;
	}
	
	/**
	 * Reserves an HTTP Solr Server instance from the underlying pool of 
	 * HTTP Solr Server instances. When done with an instance, the caller
	 * should needs to call {@link #unreserveSolrServerInstance(HttpSolrServer)}
	 * @return an HTTP Solr Server instance from the underlying pool of 
	 * HTTP Solr Server instances.
	 * @throws APException propagated from {@link ObjectPool#reserve(IPoolableObjectFactory)} 
	 */
	@Override
	public final HttpSolrServer reserveSolrServerInstance() throws APException{
		return this.solrServerObjectPool.reserve(solrServerFactory);
	}
	
	/**
	 * Frees the HTTP Solr Server instance, putting it back into the pool, making it available
	 * for future use.
	 * @param httpSolrServer is the HTTP Solr Server instance.
	 * @throws APException propagated from {@link ObjectPool#unreserve(Object)} 
	 */
	@Override
	public final void unreserveSolrServerInstance(HttpSolrServer httpSolrServer) throws APException{
		this.solrServerObjectPool.unreserve(httpSolrServer);
	}

	
	@Override
	public JSONObject index(List<SolrInputDocument> docs, String indexType) throws APException{
		String baseUrl = urlResolver.resolveUrl(indexType);
		if(StringUtilities.isEmpty(baseUrl)) {
			return SolrManager.generateErrorJSON(SolrUrlResolver.ERROR_NO_CONFIGURED_URL + indexType);
		}
		HttpSolrServer httpSolrServer = reserveSolrServerInstance();
		try {
			httpSolrServer.setBaseURL(baseUrl);
			httpSolrServer.add(docs);
			if (docs.size() > 1){
				LOGGER.info(String.format("Indexed %d items in SOLR @ %s", docs.size(), baseUrl));
			} else if (LOGGER.isLoggable(Level.FINE)){
				LOGGER.fine(String.format("Indexed one item in SOLR @ %s", baseUrl));
			}
			return SolrManager.generateSuccessJSON();
		} catch(IOException | SolrServerException e) {
			LOGGER.log(Level.SEVERE, "Index operation failed", e);
			return SolrManager.generateErrorJSON(e.getMessage());
		} finally {
			unreserveSolrServerInstance(httpSolrServer);
		}
		
	}

	@Override
	public void commit(String indexType) throws APException{
		
		String url = urlResolver.resolveUrl(indexType);
		if(StringUtilities.isEmpty(url)) {
			throw new APException(SolrUrlResolver.ERROR_NO_CONFIGURED_URL + indexType);
		}
		
		url = url + "/update?commit=true";
		try {
			HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
			conn.setRequestMethod("GET");
			int status = conn.getResponseCode();
			if(status != HTTP_SUCCESS){
				throw new APException("Commit index failed");
			}
		} catch (SecurityException | IOException e) {
			throw new APException(e.getMessage());
		}
	}
	
	@Override
	public JSONObject search(SolrQuery query, String indexType) throws APException{		
		String baseUrl = urlResolver.resolveUrl(indexType);
		if(StringUtilities.isEmpty(baseUrl)) {
			return SolrManager.generateErrorJSON(SolrUrlResolver.ERROR_NO_CONFIGURED_URL + indexType);
		}
		
		// Apply security settings
		applySecurityFilter(query, indexType);
		
		Iterator<String> iter = query.getParameterNamesIterator();
		StringBuilder url = new StringBuilder(baseUrl);
		if(url.charAt(url.length()-1) == '/'){
			url.append("search");
		}else{
			url.append("/search");	
		}
		
		
		try {
			boolean first = true;
			while(iter.hasNext()) {
				String name = iter.next();
				String value = URLEncoder.encode(query.get(name), "UTF-8");
				
				if("sort".equals(name) && null != value){
					value = value.replaceFirst("entity_name", "entity_name_sort");
				}
				
				if(first) {
					url.append(String.format("?%s=%s", name, value));
					first = false;
				}
				else {
					url.append(String.format("&%s=%s", name, value));
				}
			}
			LOGGER.fine("Solr Search url: " + url.toString());
			HttpURLConnection conn = (HttpURLConnection) new URL(url.toString()).openConnection();
			conn.setRequestMethod("GET");
			JSONObject json = new JSONObject();
			int status = conn.getResponseCode();
			
			if(status == HTTP_SUCCESS) {
				try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
					StringBuilder data = new StringBuilder();
					String temp;
					while((temp = br.readLine()) != null) {
						data.append(temp);
					}
					
					try {
						json = new JSONObject(data.toString());
						processResponse(json,indexType);
					}
					catch(JSONException e) {
						ExceptionLogger.log(e, getClass(), "search");
						return SolrManager.generateErrorJSON("Unable to successfully complete the Solr request.");
					}
				}
			}
			return json;
		} catch (SecurityException | IOException e) { //NOSONAR
			ExceptionLogger.log(e, getClass(), "search");
			return SolrManager.generateErrorJSON("Unable to successfully complete the Solr request.");
		}
	}

	@Override
	public JSONObject delete(List<String> ids, String indexType)  throws APException{
		String baseUrl = urlResolver.resolveUrl(indexType);
		if(StringUtilities.isEmpty(baseUrl)) {
			String error = SolrUrlResolver.ERROR_NO_CONFIGURED_URL + indexType;
			LOGGER.severe(error);
			return SolrManager.generateErrorJSON(error);
		}
		
		HttpSolrServer httpSolrServer = reserveSolrServerInstance();
		try {
			httpSolrServer.setBaseURL(baseUrl);
			httpSolrServer.deleteById(ids);
			if (ids.size() > 1){
				LOGGER.info(String.format("Deleting %d items from SOLR @ %s", ids.size(), baseUrl));
			} else if (LOGGER.isLoggable(Level.FINE)){
				LOGGER.fine(String.format("Deleted one item from SOLR @ %s", baseUrl));
			}
			return SolrManager.generateSuccessJSON();
		} catch(IOException | SolrServerException e) {
			LOGGER.log(Level.SEVERE, "Delete operation failed", e);
			return SolrManager.generateErrorJSON(e.getMessage());
		} finally {
			unreserveSolrServerInstance(httpSolrServer);
		}
	}
	
	@Override
	public JSONObject deleteAll(String indexType)  throws APException{
		String baseUrl = urlResolver.resolveUrl(indexType);
		if(baseUrl == null) {
			String error = SolrUrlResolver.ERROR_NO_CONFIGURED_URL + indexType;
			LOGGER.severe(error);
			return SolrManager.generateErrorJSON(error);
		}
		HttpSolrServer httpSolrServer = reserveSolrServerInstance();
		try {
			httpSolrServer.setBaseURL(baseUrl);
			httpSolrServer.deleteByQuery("*:*");
			LOGGER.info(String.format("Deleting all items from SOLR @ %s", baseUrl));
			return SolrManager.generateSuccessJSON();
		} catch(IOException | SolrServerException e) {
			return SolrManager.generateErrorJSON(e.getMessage());
		} finally {
			unreserveSolrServerInstance(httpSolrServer);
		}
	}
	
	/**
	 * Generates a JSONObject representing an error condition that occurred.
	 * 
	 * @param error		The string description of the error.
	 * @return			A JSONObject describing the error status and error that occurred.
	 */
	public static JSONObject generateErrorJSON(String error) {
		JSONObject json = new JSONObject();
		JSONObject responseHeader = new JSONObject();
		try {
			responseHeader.put("status", SOLR_ERROR_STATUS);
			responseHeader.put("description", error);
			json.put("responseHeader", responseHeader);
		} catch (JSONException e) {
			ExceptionLogger.log(e, SolrManager.class, "generateErrorJSON");
		}
		
		return json;
	}
	
	/**
	 * Generates a JSONObject representing a successful Solr interaction.
	 * 
	 * @return			A JSONObject the with success status.
	 */
	public static JSONObject generateSuccessJSON() {
		JSONObject json = new JSONObject();
		JSONObject responseHeader = new JSONObject();
		try {
			responseHeader.put("status", SOLR_SUCCESS_STATUS);
			json.put("responseHeader", responseHeader);
		} catch (JSONException e) {
			ExceptionLogger.log(e, SolrManager.class, "generateSuccessJSON");
		}
		
		return json;
	}
	
	@Override
	public SolrQuery createSolrQueryFromParams(Map<String, String[]> requestParams) {
		SolrQuery query = new SolrQuery();
		
		Iterator<Entry<String, String[]>> iter = requestParams.entrySet().iterator();
		while(iter.hasNext()) {
			Entry<String, String[]> entry = iter.next();
			String key = entry.getKey();
			if(key.matches(SolrManager.LOGON_ID_PARAM + "|" + SolrManager.XML_PARAM + "|" + SolrManager.INDEX_TYPE_PARAM)) {
				continue;
			}
			
			query.add(key, entry.getValue()[0]);
		}
		query.add("wt", "json");
		
		return query;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public void processResponse(JSONObject response,String indexType) throws APException{
		DatabaseResourceAgent dra = new DatabaseResourceAgent(this);
		try {
			if(response != null && !response.isNull("response") && indexType.equals(SolrManager.INDEX_TYPE_WORKLIST)){
				JSONObject resObj = response.getJSONObject("response");
				JSONArray docs = resObj.getJSONArray("docs");
				int count = docs.length();
				//decorate the first 4 results with actions
				for(int i=0; i<count; i++){
					JSONObject doc = docs.getJSONObject(i);
					//decorate the first 4 results with actions
					String lobCd = doc.getString("lob");
					String lobDesc = LOBCode.getLOBType(lobCd).getDescription();
					if(doc.has("workitem_status")){
						String statusMemonic = doc.getString("workitem_status");
						IWorkItemStatus wiStatus = workItemStatusManager.get(statusMemonic);
						doc.put("status_title", wiStatus.getAfterChangeStatusTitle());
					}
					
					doc.put("lob_desc", lobDesc);
					if(i<4){
						int id = doc.getInt("id");
						ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
						IWorkItemActionResolver workItemActionResolver = WorkItemFactory.getWorkItemActionResolver(WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE);
						IWorkItemManager wim = WorkItemFactory.createWorkItemManager(WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE);
						IWorkItem workItem = wim.read(dra.getConnection(true), new Id(id), securityProfile);
						WorkItemOpenMode workItemOpenMode = workItemActionResolver.getWorkItemOpenMode(securityProfile, workItem);
						doc.put("open_mode", workItemOpenMode.getOpenMode());
					}
				}
			}
		} catch (JSONException| SQLException e) {
			throw new APException(e);
		}finally{
			dra.closeDatabaseResources(this);
		}
	}

	@Override
	public void applySecurityFilter(SolrQuery query, String indexType) {
		if(AccountManagementFeature.get().isSupportGroupAccess()){
			ISecurityProfile securityProfile = SecurityProfileManager.get().acquire();
			IUserGroups userGroups = securityProfile.getUserGroups();
			if(null != userGroups){
				Set<Id> ids = userGroups.getUserGroupIds(securityProfile.getSubject().getPrincipal());
				StringBuffer filter = new StringBuffer();
				
				filter.append("(");
				Iterator<Id> it = ids.iterator();
				while(it.hasNext()){
					filter.append(it.next());
					if(it.hasNext()){
						filter.append(" OR ");
					}
				}
				filter.append(")");
				
				String q = query.get("q");
				q += " AND usergroupid:" + filter.toString();
				query.set("q", q);
			}
		}
	}
	
	/**
	 * Formats a Date object so that it is compatible with Solr.
	 * @param date	The Date object to format.
	 * @return A Solr-compatible String representation of the Date object provided.
	 */
	public static String formatDate(Date date) {
		SimpleDateFormat formattedDate = new SimpleDateFormat(UTC_DATE_FORMAT);
		formattedDate.setTimeZone(TimeZone.getTimeZone("UTC"));
		return formattedDate.format(date);
	}

	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public void release() throws APException {
		solrServerObjectPool.release();
	}

	/**
	 * Returns a direct reference to the Solr URL resolver.
	 * @return a direct reference to the Solr URL resolver.
	 */
	@Override
	public ISolrUrlResolver getUrlResolver() {
		return urlResolver;
	}
}
