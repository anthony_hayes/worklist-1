package com.agencyport.worklist.solr.manager;

import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;
import org.json.JSONObject;

import com.agencyport.pool.IPoolableObjectFactory;
import com.agencyport.pool.ObjectPool;
import com.agencyport.shared.APException;

/**
 * Provides the portal's API into the Solr search index/datastore. It can (and should) be used for making update, delete, search and index
 * requests into Solr. Its default implementation (the @link {@link SolrManager} class) does all of the work of working with apache's 
 * solrJ client for interfacing with Solr and providing the necessary security tokens under the hood. To get an instance, use 
 * {@link SolrManager#getSolrManager()}
 * @since 5.0
 */
public interface ISolrManager {

	/**
	 * Method to construct a request to the Solr REST interface to perform indexing of a list of documents.
	 * 
	 * @param docs				The list of documents to be indexed.
	 * @param indexType			The index to insert or update on.
	 * @return					A JSON object representing the result of the request. 
	 * @throws APException		Is the exception thrown
	 */
	JSONObject index(List<SolrInputDocument> docs, String indexType) throws APException;
	
	/**
	 * Method to construct a request to the Solr REST interface to search the index.
	 * 
	 * @param query				A SolrQuery object representing the query to perform.
	 * @param indexType			The index to query against.
	 * @return					A JSON object representing the result of the request.
	 * @throws APException		Is the exception thrown
	 */
	JSONObject search(SolrQuery query, String indexType) throws APException;
	
	/**
	 * Method to construct a request to the Solr REST interface to delete an indexed document.
	 * 
	 * @param ids				The list of document ids to delete.
	 * @param indexType			The index that we are deleting from.
	 * @return					A JSON object representing the result of the request.
	 * @throws APException		Is the exception thrown
	 */
	JSONObject delete(List<String> ids, String indexType) throws APException;
	
	/**
	 * Method to construct a request to the Solr REST interface to delete all documents for an index.
	 * 
	 * @param indexType			The index that we are deleting from.
	 * @return					A JSON object representing the result of the request.
	 * @throws APException		Is the exception thrown
	 */
	JSONObject deleteAll(String indexType) throws APException;
	
	/**
	 * Read in request parameters and convert them into a SolrQuery object.
	 * 
	 * @param requestParams		The request parameters to evaluate.
	 * @return					A SolrQuery instance that contains query attributes based on the request parameters.
	 */
	SolrQuery createSolrQueryFromParams(Map<String, String[]> requestParams);
	
	/**
	 * Provides a hook to perform additional processing and manipulation on the response from Solr.
	 * 
	 * @param response	The JSONObject representing the response from Solr.
	 * @param indexType	is the index that is being processed
	 * @throws APException		Is the exception thrown
	 */
	void processResponse(JSONObject response, String indexType) throws APException;
	
	/**
	 * Applies the configured ACSI security policy for this user to the Solr query.
	 * 
	 * @param query				The SolrQuery object to apply the security policy to.
	 * @param indexType	is the index that is being processed
	 */
	void applySecurityFilter(SolrQuery query, String indexType);
	
	/**
	 * Commit pending documents or updates to make them available for search
	 * 
	 * @param indexType			The index that we are deleting from.
	 * @throws APException		Is the exception thrown
	 */
	void commit(String indexType) throws APException;
	/**
	 * Reserves an HTTP Solr Server instance from the underlying pool of 
	 * HTTP Solr Server instances. When done with an instance, the caller
	 * should needs to call {@link #unreserveSolrServerInstance(HttpSolrServer)}
	 * @return an HTTP Solr Server instance from the underlying pool of 
	 * HTTP Solr Server instances.
	 * @throws APException propagated from {@link ObjectPool#reserve(IPoolableObjectFactory)} 
	 */
	HttpSolrServer reserveSolrServerInstance() throws APException;
	/**
	 * Frees the HTTP Solr Server instance, putting it back into the pool, making it available
	 * for future use.
	 * @param httpSolrServer is the HTTP Solr Server instance.
	 * @throws APException propagated from {@link ObjectPool#unreserve(Object)} 
	 */
	void unreserveSolrServerInstance(HttpSolrServer httpSolrServer) throws APException;

	ISolrUrlResolver getUrlResolver();
}
