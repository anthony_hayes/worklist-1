/*
 * Created on Apr 17, 2014 by dan AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.solr.manager;

/**
 * The ISolrUrlResolver class is the interface for concrete implementations that will
 * resolve Solr urls based on the index type.
 * @since 5.0
 */
public interface ISolrUrlResolver {

	/**
	 * Resolves the URL for the Solr index based on the index type passed in.
	 * 
	 * @param indexType		The index type whose url we are looking for.
	 * @return				The url of the desired index, or null if there is no match.
	 */
	String resolveUrl(String indexType);
}
