/*
 * Created on Mar 15, 2016 by ldeane AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.logging.ExceptionLogger;
import com.agencyport.utils.AppProperties;


/**
 * The WorkItemContent class models the content of an IWorkItem implementation
 */
@XmlRootElement
public class WorkItemContent {
	
	/**
	 * The <code>APPLICATION_WORKITEM_CONTENT_CLASSNAME</code> is the property name for obtaining the class name
	 * for an application's extension of the WorkItemContent class.
	 */
	private static final String APPLICATION_WORKITEM_CONTENT_CLASSNAME = "application.workitem_content_classname";
	
	/**
	 * The <code>id</code> is the Id of the associated IWorkItem
	 */
	private String id;
	
	/**
	 * The <code>properties</code> contains all Property contents
	 */
	private List<Property> properties = new ArrayList<Property>();
	
	/**
	 * Constructs an instance
	 */
	public WorkItemContent(){
		this.setId(null);
		this.setProperties(new ArrayList<Property>());
	}

	/**
	 * Returns the id
	 * @return this.id
	 */
	@XmlAttribute(name="id")
	public String getId() {
		return id;
	}

	/**
	 * Sets the id
	 * @param id is the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Retrieves the properties of the associated IWorkItem instance
	 * @return List containing the properties of the associated IWorkItem instance
	 */
	@XmlElements(@XmlElement(name="property", type=Property.class))
	public List<Property> getProperties() {
		return properties;
	}

	/**
	 * Sets the properties
	 * @param properties is the List<Property> to set
	 */
	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}
	

	
	/**
	 * Returns the class name of the WorkItemContent class derivation.
	 * @return the class name of the WorkItemContent class derivation.
	 */
	@SuppressWarnings("unchecked")
	public static Class<? extends WorkItemContent> obtainClassInfo(){
		String className = AppProperties.getAppProperties().getStringProperty(APPLICATION_WORKITEM_CONTENT_CLASSNAME, WorkItemContent.class.getName());
		try {
			return (Class<? extends WorkItemContent>) Class.forName(className);
		} catch (ClassNotFoundException e) {
			ExceptionLogger.log(e, WorkItemContent.class, "obtainClassInfo");
			throw new IllegalStateException(e);
		}
	}

}
