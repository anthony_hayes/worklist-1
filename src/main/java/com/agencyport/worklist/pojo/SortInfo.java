/*
 * Created on Dec 3, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.api.URIBuilder;
import com.agencyport.api.pojo.ItemLink;
import com.agencyport.worklist.api.AccountListResource;
import com.agencyport.worklist.api.WorkItemListResource;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.IConstants;
import com.agencyport.rest.ParameterNames;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.agencyport.workitem.model.WorkItemType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The SortInfo class is the POJO class supporting the exchange of sort info meta data.
 * @since 5.1
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public final class SortInfo extends ItemLink implements Cloneable {
	/**
	 * The <code>sortOption</code> are the associated sort options.
	 */
	private List<SortOption> sortOptions;
	/**
	 * Constructs an empty instance.
	 */
	public SortInfo() {
	}
	/**
	 * Constructs an instance.
	 * @param name see {@link #name}
	 * @param title see {@link #title}
	 * @param link see {@link #link}
	 */
	public SortInfo(String name, String title, AtomLink link){
		super(name, title, link);
	}
	
	/**
	 * Constructs an instance with just a name.
	 * @param name is the name.
	 */
	public SortInfo(String name){
		super(name, null, null);
	}
	/**
	 * Returns the sort options.
	 * @return the sort options.
	 */
	@XmlElementWrapper(name="sortOptions")
	@XmlElements(@XmlElement(name="sortOption", type=SortOption.class))
	@JsonProperty("sortOptions")
	public List<SortOption> getSortOptions() {
		return sortOptions;
	}
	/**
	 * Sets the sort options.
	 * @param sortOptions are the sort options to set.
	 */
	public void setSortOptions(List<SortOption> sortOptions) {
		this.sortOptions = sortOptions;
	}
	/**
	 * Builds a SortInfo instance.
	 * @param name is the name of the sort.
	 * @param title is the title of the sort.
	 * @param workItemType is the work item type.
	 * @param view is the view.
	 * @return a SortInfo instance. 
	 */
	public static SortInfo create(String name, String title, WorkItemType workItemType, String view){
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put(IConstants.VIEW_NAME, view);
		Class<?> relatedClass = workItemType.equals(WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE) ? WorkItemListResource.class : AccountListResource.class;  
		AtomLink link = new AtomLink(URIBuilder.createURI(relatedClass, IConstants.SORT_INFO_PATH, null, queryParams), ParameterNames.create(IConstants.VIEW_NAME));
		return new SortInfo(name, title, link);
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		return super.toString() + ", " + SortInfo.class.getSimpleName() + " [sort options =" + sortOptions + "]";
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public boolean equals(Object other){
		if (other instanceof SortInfo){
			SortInfo item = (SortInfo) other;
			return super.equals(other) && EqualityChecker.areEqual(this.sortOptions, item.sortOptions);
		} else {
			return false;
		}
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public int hashCode(){
		return super.hashCode() + HashCodeCalculator.calculate(this.sortOptions);
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public Object clone() throws CloneNotSupportedException {
		SortInfo copy = (SortInfo) super.clone();
		copy.sortOptions = new ArrayList<>(this.sortOptions);
		return copy;
	}
}
