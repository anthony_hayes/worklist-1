/*
 * Created on Feb 5, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.api.URIBuilder;
import com.agencyport.worklist.api.AccountListResource;
import com.agencyport.worklist.api.WorkItemListResource;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.IConstants;
import com.agencyport.rest.ParameterNames;
import com.agencyport.workitem.model.WorkItemType;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The SavedSearchInfo class is the container for the saved search records.
 * @since 5.1
 */
@XmlRootElement
public class SavedSearchInfo implements Cloneable  {
	/**
	 * The <code>link</code> is the link on how to query for for the saved searches.
	 */
	private AtomLink link;
	
	/**
	 * The <code>savedSearches</code> includes all of the saved search information instances for this work list view for this user.
	 */
	private List<SavedSearch> savedSearches = new ArrayList<>();
	
	
	/**
	 * The <code>currentSearch</code> is the search information for the current work list view. 
	 */
	private SavedSearch currentSearch = new SavedSearch();
	


	/**
	 * Creates an instance.
	 */
	public SavedSearchInfo() {
	}
	
	/**
	 * Creates a saved searches instance and initializes the link.
	 * @param workItemType is the work item type.
	 * @param view is the view name.
	 * @return a saved searches instance and initializes the link.
	 */
	public static SavedSearchInfo create(WorkItemType workItemType, String view){
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put(IConstants.VIEW_NAME, view);
		Class<?> relatedClass = workItemType.equals(WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE) ? WorkItemListResource.class : AccountListResource.class;  
		AtomLink link = new AtomLink(URIBuilder.createURI(relatedClass, IConstants.SAVED_SEARCHES_PATH, null, queryParams), ParameterNames.create(IConstants.VIEW_NAME, IConstants.SAVED_SEARCH_ID));
		SavedSearchInfo savedSearchInfo =  new SavedSearchInfo();
		savedSearchInfo.setLink(link);
		return savedSearchInfo;
	}
	/**
	 * Gets the link for this item.
	 * @return the link
	 */
	@XmlElement(name=AtomLink.LINK)
	public AtomLink getLink() {
		return link;
	}
	/**
	 * Sets the link for this item.
	 * @param link the link to set
	 */
	public void setLink(AtomLink link) {
		this.link = link;
	}
	/**
	 * @return the savedSearches
	 */
	@XmlElementWrapper(name="savedSearches")
	@XmlElements(@XmlElement(name="savedSearch", type=SavedSearch.class))
	@JsonProperty("savedSearches")
	public List<SavedSearch> getSavedSearches() {
		return savedSearches;
	}
	/**
	 * @param savedSearches the savedSearches to set
	 */
	public void setSavedSearches(List<SavedSearch> savedSearches) {
		this.savedSearches = savedSearches;
	}
	/**
	 * @return the savedSearch
	 */
	@XmlElement(name="currentSearch")
	@JsonProperty("currentSearch")
	public SavedSearch getCurrentSearch() {
		return currentSearch;
	}
	/**
	 * @param currentSavedSearch the currentSavedSearch to set
	 */
	public void setCurrentSearch(SavedSearch currentSavedSearch) {
		this.currentSearch = currentSavedSearch;
	}

	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		return "SavedSearchInfo [link=" + link + ", "
				+ "savedSearches=" + savedSearches + 
				", currentSearch=" + currentSearch + 
				"]";
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public Object clone() throws CloneNotSupportedException {
		SavedSearchInfo copy = (SavedSearchInfo) super.clone();
		copy.savedSearches = new ArrayList<>(this.savedSearches);
		return copy;
	}

}
