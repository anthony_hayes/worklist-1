/*
 * Created on Jan 2, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;

import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The ListView class models a work list view.
 * @since 5.1
 */
public final class ListView implements Cloneable {
	/**
	 * The <code>fields</code> contains the list of fields on the view.
	 */
	private List<Field> fields;
	
	/**
	 * The <code>type</code> is the type of this view.
	 */
	private ListType type;
	
	/**
	 * The <code>DEFAULT_VIEWPORT_SIZE</code> defines the default view port size.
	 */
	public static final int DEFAULT_VIEWPORT_SIZE = 60;
	/**
	 * The <code>viewPortSize</code> contains the number of rows to return
	 * when a query is run.
	 */
	private int viewPortSize = DEFAULT_VIEWPORT_SIZE;
	/**
	 * The <code>startRowNumber</code> designates the starting row number to return.
	 */
	private int startRowNumber;
	/**
	 * The <code>selected</code> determines whether this list view is selected as the active list view.
	 */
	private boolean selected = false;
	/**
	 * The <code>DEFAULT_FETCH_SIZE</code> defines the default fetch size.
	 */
	public static final int DEFAULT_FETCH_SIZE = 60;

	/**
	 * The <code>fetchSize</code> designates how many rows to fetch in one search request.
	 */
	private int fetchSize = DEFAULT_FETCH_SIZE;
	/**
	 * Constructs an instance.
	 */
	public ListView() {
	}
	/**
	 * @return the fields
	 */
	@XmlElementWrapper(name="fields")
	@XmlElements(@XmlElement(name="field", type=Field.class))
	@JsonProperty("fields")
	public List<Field> getFields() {
		return fields;
	}
	/**
	 * @param fields the fields to set
	 */
	public void setFields(List<Field> fields) {
		this.fields = fields;
	}
	
	/**
	 * Get field for a given field reference id
	 * @param fieldRefId is the id for which field is being looked up
	 * @return Field instance
	 */
	public Field getField(String fieldRefId){
		for(Field field: getFields()){
			if(field.getId().equals(fieldRefId)){
				return field;
			}
		}
		return null;
	}
	/**
	 * @return the type
	 */
	@XmlAttribute(name="type")
	@JsonProperty("type")
	public ListType getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(ListType type) {
		this.type = type;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		return "ListView [fields=" + fields + 
				", type=" + type + 
				", selected=" + selected + 
				", startRowNumber=" + startRowNumber + 
				", viewPortSize=" + viewPortSize + 
				", fetchSize=" + fetchSize + 
				"]";
	}
	/**
	 * @return the viewPortSize
	 */
	@XmlAttribute(name="viewPortSize")
	@JsonProperty("viewPortSize")
	public int getViewPortSize() {
		return viewPortSize;
	}

	/**
	 * @param viewPortSize the viewPortSize to set
	 */
	public void setViewPortSize(int viewPortSize) {
		this.viewPortSize = viewPortSize;
	}

	/**
	 * @return the startRowNumber
	 */
	@XmlAttribute(name="startRowNumber")
	@JsonProperty("startRowNumber")
	public int getStartRowNumber() {
		return startRowNumber;
	}

	/**
	 * @param startRowNumber the startRowNumber to set
	 */
	public void setStartRowNumber(int startRowNumber) {
		this.startRowNumber = startRowNumber;
	}
	/**
	 * Returns whether this item is selected or not.
	 * @return true if this item is selected.
	 */
	@XmlAttribute(name="selected")
	@JsonProperty("selected")
	public boolean getSelected(){
		return selected;
	}
	
	/**
	 * Sets the selected flag.
	 * @param selected is the selected flag to set.
	 */
	public void setSelected(boolean selected){
		this.selected = selected;
	}
	/**
	 * @return the fetchSize
	 */
	@XmlAttribute(name="fetchSize")
	@JsonProperty("fetchSize")
	public int getFetchSize() {
		return fetchSize;
	}
	/**
	 * @param fetchSize the fetchSize to set
	 */
	public void setFetchSize(int fetchSize) {
		this.fetchSize = fetchSize;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public boolean equals(Object other){
		if (other instanceof ListView){
			ListView item = (ListView) other;
			return EqualityChecker.areEqual(this.type, item.type, this.fields, item.fields);
		} else {
			return false;
		}
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public int hashCode(){
		return super.hashCode() + HashCodeCalculator.calculate(this.type, this.fields);
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public Object clone() throws CloneNotSupportedException {
		ListView copy = (ListView) super.clone();
		copy.fields = new ArrayList<>(this.fields);
		return copy;
	}

}
