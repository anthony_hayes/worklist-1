/*
 * Created on Dec 10, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.api.URIBuilder;
import com.agencyport.api.pojo.DataType;
import com.agencyport.api.pojo.ItemLink;
import com.agencyport.locale.ILocaleConstants;
import com.agencyport.locale.IResourceBundle;
import com.agencyport.locale.ResourceBundleManager;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.IConstants;
import com.agencyport.utils.HashCodeCalculator;
import com.agencyport.utils.text.StringUtilities;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.api.AccountListResource;
import com.agencyport.worklist.api.WorkItemListResource;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Operators class provides a mapping between a data type and its various op codes. This is not singleton
 * but is not designed to be de-serialized. It can only be JSON serialized and not XML serialized. 
 * @since 5.1 
 */
@XmlRootElement
public final class Operators extends ItemLink{
	
	/**
	 * The <code>RESOURCE_BUNDLE_LOOKUP_PATTERN</code> is the pattern used for the resource bundle look up
	 * for the operator localized title.
	 */
	private static final String RESOURCE_BUNDLE_LOOKUP_PATTERN = "title.datatype.%s.opCode.%s";
	/**
	 * The <code>operators</code> contains the map of supported operators by data type.
	 */
	private final Map<DataType, List<Operator>> operators = new HashMap<>();
	
	
	/**
	 * Constructs an empty instance.
	 */
	public Operators(){
		this(null);
	}
	/**
	 * Constructs an instance.
	 * @param link see {@link #link}
	 */
	public Operators(AtomLink link){
		super("operatorinfo", "operators by data type", link);
		IResourceBundle rb = ResourceBundleManager.get().getBundle(ILocaleConstants.WORKLIST_BUNDLE);
		for (DataType dataType : DataType.values()){
			List<Operator> operatorList = new ArrayList<>();
			operators.put(dataType, operatorList);
			for (OpCode opCode : OpCode.values()){
				String rbLookup = String.format(RESOURCE_BUNDLE_LOOKUP_PATTERN, dataType, opCode.getValue());
				String operatorTitle = rb.getString(rbLookup, null);
				if (!StringUtilities.isEmpty(operatorTitle)){
					operatorList.add(new Operator(operatorTitle, opCode));
				}
			}
		}
	}
	/**
	 * Creates an instance.
	 * @param workItemType is the work item type.
	 * @return an Operators instance with localized titles.
	 */
	public static Operators create(WorkItemType workItemType){
		Class<?> relatedClass = workItemType.equals(WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE) ? WorkItemListResource.class : AccountListResource.class;  
		AtomLink link = new AtomLink(URIBuilder.createURI(relatedClass, IConstants.OPERATORS_INFO_PATH));
		return new Operators(link);
	}
	/**
	 * @return the operators
	 */
	@JsonProperty("operators")
	public Map<DataType, List<Operator>> getOperators() {
		return operators;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public boolean equals(Object other){
		if (other instanceof Operators){
			return super.equals(other);
		} else {
			return false;
		}
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public int hashCode(){
		return super.hashCode() + HashCodeCalculator.calculate(this.getClass());
	}
}
