/**
 * POJO classes supporting of work list entities.  
 * @since 5.1
 */
package com.agencyport.worklist.pojo;