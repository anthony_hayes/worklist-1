/*
 * Created on Dec 2, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.api.URIBuilder;
import com.agencyport.api.pojo.ItemLink;
import com.agencyport.api.worklist.pojo.FilterType;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.IConstants;
import com.agencyport.rest.ParameterNames;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.api.AccountListResource;
import com.agencyport.worklist.api.WorkItemListResource;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Filter class is the POJO class supporting the exchange of filter meta data.
 * @since 5.1
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Filter extends ItemLink {
	/**
	 * The <code>filterOptions</code> are the filter options.
	 */
	private List<FilterOption> filterOptions;
	
	/**
	 * The <code>styleClass</code> is the CSS style name.
	 */
	private String styleClass;
	
	/**
	 * The <code>format</code> is the format mask.
	 */
	private String format;
	
	/**
	 * The <code>relatedFieldId</code> is the id of the related field.
	 */
	private String relatedFieldId;
	
	/**
	 * The <code>type</code> is the type of filter.
	 */
	private FilterType type;
	/**
	 * The <code>isSaveable</code> indicates if this filter can be saved by save search operation
	 */
	private boolean isSaveable;
	/**
	 * Constructs an empty instance.
	 */
	public Filter() {
	}
	/**
	 * Constructs an instance.
	 * @param name see {@link #name}
	 * @param title see {@link #title}
	 * @param link see {@link #link}
	 */
	public Filter(String name, String title, AtomLink link){
		super(name, title, link);
	}
	
	/**
	 * Constructs an instance with just a name.
	 * @param name is the name.
	 */
	public Filter(String name){
		this(name, null, null);
	}
	/**
	 * Returns the filter options.
	 * @return the filter options.
	 */
	@XmlElementWrapper(name="filterOptions")
	@XmlElements(@XmlElement(name="filterOption", type=FilterOption.class))
	@JsonProperty("filterOptions")
	public List<FilterOption> getFilterOptions() {
		return filterOptions;
	}
	/**
	 * Sets the filter options.
	 * @param filterOptions are the filter options to set.
	 */
	public void setFilterOptions(List<FilterOption> filterOptions) {
		this.filterOptions = filterOptions;
	}
	/**
	 * Builds a Filter instance.
	 * @param name is the name of the filter.
	 * @param title is the title of the filter.
	 * @param workItemType is the work item type.
	 * @param view is the view.
	 * @return a Filter instance. 
	 */
	public static Filter create(String name, String title, WorkItemType workItemType, String view){
		Map<String, String> pathParams = new HashMap<>();
		pathParams.put(IConstants.FILTER_NAME, name);
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put(IConstants.VIEW_NAME, view);
		Class<?> relatedClass = workItemType.equals(WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE) ? WorkItemListResource.class : AccountListResource.class;  
		AtomLink link = new AtomLink(URIBuilder.createURI(relatedClass, IConstants.FILTER_PATH, pathParams, queryParams), ParameterNames.create(IConstants.VIEW_NAME));
		return new Filter(name, title, link);
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public String toString() {
		return super.toString() + 
				" [filter options =" + filterOptions + 
				", styleClass=" + styleClass + 
				", format=" + format + 
				", relatedFieldName=" + relatedFieldId + 
				"]";
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public boolean equals(Object other){
		if (other instanceof Filter){
			Filter item = (Filter) other;
			return super.equals(other) && EqualityChecker.areEqual(this.format, item.format, 
					this.styleClass, item.styleClass, 
					this.filterOptions, item.filterOptions, 
					this.relatedFieldId, item.relatedFieldId,
					this.type, item.type);
		} else {
			return false;
		}
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public int hashCode(){
		return super.hashCode() + HashCodeCalculator.calculate(this.format, 
				this.styleClass, 
				this.filterOptions, 
				this.relatedFieldId,
				this.type);
	}
	/**
	 * @return the styleClass
	 */
	@XmlAttribute(name="styleClass")
	@JsonProperty("styleClass")
	public String getStyleClass() {
		return styleClass;
	}

	/**
	 * @param styleClass the styleClass to set
	 */
	public void setStyleClass(String styleClass) {
		this.styleClass = styleClass;
	}

	/**
	 * @return the format
	 */
	@XmlAttribute(name="format")
	@JsonProperty("format")
	public String getFormat() {
		return format;
	}

	/**
	 * @param format the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}
	/**
	 * @return the relatedFieldName
	 */
	@XmlAttribute(name="relatedFieldId")
	@JsonProperty("relatedFieldId")
	public String getRelatedFieldId() {
		return relatedFieldId;
	}
	/**
	 * @param relatedFieldId the relatedFieldId to set
	 */
	public void setRelatedFieldId(String relatedFieldId) {
		this.relatedFieldId = relatedFieldId;
	}
	/**
	 * @return the type
	 */
	@XmlAttribute(name="type")
	@JsonProperty("type")
	public FilterType getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(FilterType type) {
		this.type = type;
	}

	/**
	 * @return isSaveable filter boolean indicator
	 */
	@XmlAttribute(name="isSaveable")
	@JsonProperty("isSaveable")
	public boolean isSaveable() {
		return isSaveable;
	}

	/**
	 * Set the can be saved boolean indicator
	 * @param isSaveable is the flag indicating if this filter can be saved.
	 */
	public void setSaveable(boolean isSaveable) {
		this.isSaveable = isSaveable;
	}	
}
