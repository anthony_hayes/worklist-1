/*
 * Created on Jan 8, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import java.util.List;

import com.agencyport.api.pojo.DataType;
import com.agencyport.customlists.ICustomListBuilder;
import com.agencyport.data.DataManager;
import com.agencyport.html.optionutils.OptionEntry;
import com.agencyport.html.optionutils.OptionList;
import com.agencyport.html.optionutils.OptionList.Key;

/**
 * The OperatorsListBuilder class provides the custom list builder implementation for operators for a given data type.
 * The data type can be conveyed in the target attribute on the optionList element. An example is target="dataType=date".
 * @since 5.1
 */
public final class OperatorsListBuilder implements ICustomListBuilder {

	/**
	 * Constructs an instance.
	 */
	public OperatorsListBuilder() {
	}

	/** 
	 * {@inheritDoc}
	 */

	@Override
	public OptionList generate(DataManager dataManager, Key key, OptionList existingListToUpdate) {
		String target = key.getTarget();
		String[] tokens = target.split(",");
		DataType dataType = DataType.STRING;
		for (String token : tokens){
			int equalsPos = token.indexOf('=');
			if (equalsPos > -1){
				String name = token.substring(0, equalsPos);
				String value = token.substring(equalsPos + 1);
				if ("dataType".equals(name)){
					dataType = DataType.valueOf(value);
				}
			}
		}
		OptionList operatorsList = new OptionList(key, dataType.name(), null);
		if (existingListToUpdate != null){
			operatorsList.addAll(existingListToUpdate);
		}		
		Operators operators = new Operators();
		List<Operator> configuredOperators = operators.getOperators().get(dataType);
		for (Operator operator : configuredOperators){
			operatorsList.add(new OptionEntry(operator.getOpCode().getValue(), operator.getTitle()));
		}
		return operatorsList;
	}

	/** 
	 * {@inheritDoc}
	 */

	@Override
	public String getDefaultValue(DataManager dataManager, Key key, OptionList displayList) {
		return null;
	}

}
