/*
 * Created on Dec 5, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.agencyport.api.URIBuilder;
import com.agencyport.api.pojo.Item;
import com.agencyport.api.pojo.ItemLink;
import com.agencyport.worklist.api.AccountListResource;
import com.agencyport.worklist.api.WorkItemListResource;
import com.agencyport.factory.GenericFactory;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.rest.AtomLink;
import com.agencyport.rest.IConstants;
import com.agencyport.shared.APException;
import com.agencyport.utils.AppProperties;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.agencyport.workitem.model.WorkItemType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The WorkListView class is an aggregation of filtering, sorting and paging information for
 * governing work list queries. Derivations of this class should be configured
 * in application properties under the property name of <b>application.worklist_view_classname</b>.
 */
@XmlRootElement
@SuppressWarnings("unchecked")
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkListView extends ItemLink implements Cloneable {
	/**
	 * The <code>APPLICATION_WORKLIST_VIEW_CLASSNAME</code> is the property name for obtaining the class name
	 * for an application's extension of the WorkListView class.
	 */
	private static final String APPLICATION_WORKLIST_VIEW_CLASSNAME = "application.worklist_view_classname";
	/**
	 * The <code>version</code> is the software version under which an instance was persisted to the database. 
	 */
	private String version;
	
	/**
	 * The <code>index</code> is the index name.
	 */
	private String index;
	
	/**
	 * The <code>isSaveable</code> is boolean indicating if the state of the view can be saved
	 */
	private boolean isSaveable;
	
	/**
	 * The <code>filters</code> contains the filter information.
	 */
	private List<Filter> filters; 
	/**
	 * The <code>sortInfo</code>
	 */
	private SortInfo sortInfo;
	/**
	 * The <code>queryInfo</code> contains the query fields.
	 */
	private QueryInfo queryInfo;
	
	/**
	 * The <code>listViews</code> is the collection of list views.
	 */
	private List<ListView> listViews;
	
	/**
	 * The <code>savedSearchInfo</code> are all the saved searches for this view name / user combination.
	 */
	private SavedSearchInfo savedSearchInfo;
	
	/**
	 * The <code>lookupLinks</code> is a set of atom links that can be used to translate code values into user friendly titles.
	 */
	private List<AtomLink> lookupLinks;
	/**
	 * Constructs an empty instance. Application should use the {@link #create()} instead. This method is here for the POJO de-serialization
	 * engines such as Jackson and JAXB. 
	 */
	public WorkListView() {
	}
	/**
	 * Creates an empty instance.
	 * @return an empty instance.
	 */
	public static WorkListView create(){
		try {
			return GenericFactory.create(APPLICATION_WORKLIST_VIEW_CLASSNAME, WorkListView.class.getName());
		} catch (APException e) {
			ExceptionLogger.log(e, WorkListView.class, "create");
			throw new IllegalStateException(e);
		}
	}
	
	/**
	 * Returns the class name of the SearchInfo class derivation.
	 * @return the class name of the SearchInfo class derivation.
	 */
	public static Class<? extends WorkListView> obtainClassInfo(){
		String className = AppProperties.getAppProperties().getStringProperty(APPLICATION_WORKLIST_VIEW_CLASSNAME, WorkListView.class.getName());
		try {
			return (Class<? extends WorkListView>) Class.forName(className);
		} catch (ClassNotFoundException e) {
			ExceptionLogger.log(e, WorkListView.class, "obtainClassInfo");
			throw new IllegalStateException(e);
		}
	}
	/**
	 * Creates an instance.
	 * @param name is the name.
	 * @param link is the atom link.
	 */
	public WorkListView(String name, AtomLink link){
		super(name, "Worklist View", link);
	}
	/**
	 * Builds a SearchInfo instance.
	 * @param workItemType is the work item type.
	 * @param view is the view.
	 * @return a SearchInfo instance. 
	 */
	public static WorkListView create(WorkItemType workItemType, String view){
		Class<?> relatedClass = workItemType.equals(WorkItemType.REGULAR_LOB_WORK_ITEM_TYPE) ? WorkItemListResource.class : AccountListResource.class;
		Map<String, String> pathParameters = new HashMap<>();
		pathParameters.put(IConstants.VIEW_NAME, view);
		Set<String> queryParameters = new HashSet<>();
		queryParameters.add(IConstants.SAVED_SEARCH_ID);
		AtomLink link = new AtomLink(URIBuilder.createURI(relatedClass, IConstants.VIEW_PATH, pathParameters), queryParameters);
		try {
			WorkListView workListView = GenericFactory.create(APPLICATION_WORKLIST_VIEW_CLASSNAME, WorkListView.class.getName(), view, link);
			workListView.setSavedSearchInfo(SavedSearchInfo.create(workItemType, view));
			return workListView;
		} catch (APException e) {
			ExceptionLogger.log(e, WorkListView.class, "create");
			throw new IllegalStateException(e);
		}
	}
	/**
	 * @return the filters*
	 */
	@XmlElementWrapper(name="filters")
	@XmlElements(@XmlElement(name="filter", type=Filter.class))
	@JsonProperty("filters")
	public List<Filter> getFilters() {
		return filters;
	}
	/**
	 * @param filters the filters to set
	 */
	public void setFilters(List<Filter> filters) {
		this.filters = filters;
	}
	/**
	 * @return the sortInfo
	 */
	@XmlElement(name="sortInfo")
	@JsonProperty("sortInfo")
	public SortInfo getSortInfo() {
		return sortInfo;
	}
	/**
	 * @param sortInfo the sortInfo to set
	 */
	public void setSortInfo(SortInfo sortInfo) {
		this.sortInfo = sortInfo;
	}
	/**
	 * @return the query info.
	 */
	@XmlElement(name="queryInfo")
	@JsonProperty("queryInfo")
	public QueryInfo getQueryInfo() {
		return queryInfo;
	}
	/**
	 * @param queryInfo is the query info to set.
	 */
	public void setQueryInfo(QueryInfo queryInfo) {
		this.queryInfo = queryInfo;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		return super.toString() + ", " + WorkListView.class.getSimpleName() +  
				" [ " + 
				"version=" + version +
				", index=" + index +
				", isSaveable=" + isSaveable + 
				", filters=" + filters + 
				", sort info=" + sortInfo + 
				", query info=" + queryInfo + 
				", list views=" + listViews + 
				", savedSearchInfo=" + savedSearchInfo + 
				", lookupLinks=" + lookupLinks + 
				"]";
	}

	/**
	 * @return the version
	 */
	@XmlAttribute(name="version")
	@JsonProperty("version")
	public String getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	
	/**
	 * Merges the passed search information onto this instance. 
	 * @param fromView is the search information packet to read.
	 */
	public void merge(WorkListView fromView){
		version = fromView.version;
		mergeFilters(fromView);
		mergeSortOptions(fromView);
		mergeQueryFields(fromView);
		mergeListViews(fromView);
		this.savedSearchInfo = fromView.savedSearchInfo;
	}
	
	/**
	 * Merges the filters. The current instance will be updated.
	 * @param workListView is the search information packet to read the filters from. 
	 */
	protected void mergeFilters(WorkListView workListView){
		Map<String, Filter> myFilters = (Map<String, Filter>) Item.makeMap(this.filters);
		for (Filter theirFilter : workListView.getFilters()){
			Filter myFilter = myFilters.get(theirFilter.getName());
			if (myFilter == null){
				continue;
			}
			mergeFilterOptions(myFilter, theirFilter);
		}
	}
	
	/**
	 * Merges two sets of filter options
	 * @param thisFilter is the filter options which will be updated.
	 * @param theirFilter is the filter options which will be read.
	 */
	protected void mergeFilterOptions(Filter thisFilter, Filter theirFilter){
		Map<String, FilterOption> myFilterOptions = (Map<String, FilterOption>) Item.makeMap(thisFilter.getFilterOptions());
		for (FilterOption theirFilterOption : theirFilter.getFilterOptions()){
			FilterOption myFilterOption = myFilterOptions.get(theirFilterOption.getKey());
			if (myFilterOption == null){
				continue;
			}
			myFilterOption.setSelected(theirFilterOption.getSelected());
		}
	}
	
	/**
	 * Merges the sort options. The current instance will be updated.
	 * @param workListView is the search information packet to read the sort options from. 
	 */
	protected void mergeSortOptions(WorkListView workListView){
		SortOption currentSelectOption = null;
		Map<String, SortOption> mySortOptions = (Map<String, SortOption>) Item.makeMap(this.getSortInfo().getSortOptions());
		for (SortOption theirSortOption : workListView.getSortInfo().getSortOptions()){
			SortOption mySortOption = mySortOptions.get(theirSortOption.getKey());
			if (mySortOption == null){
				continue;
			}
			mySortOption.setSelected(theirSortOption.getSelected());
			mySortOption.setAscending(theirSortOption.getAscending());
			currentSelectOption = theirSortOption;
		}
		//Eliminate the duplicates
		if(currentSelectOption != null){
			for (SortOption sortOption : this.getSortInfo().getSortOptions()){
				if(!sortOption.getKey().equals(currentSelectOption.getKey())){
					sortOption.setSelected(false);
				}
			}
		}
	}
	
	/**
	 * Merges the query fields. The current instance will be updated.
	 * @param workListView is the search information packet to read the query fields from. 
	 */
	protected void mergeQueryFields(WorkListView workListView){
		Map<String, QueryField> myQueryFields = (Map<String, QueryField>) Item.makeMap(this.queryInfo.getQueryFields());
		for (QueryField theirQueryField : workListView.queryInfo.getQueryFields()){
			QueryField myQueryField = myQueryFields.get(theirQueryField.getKey());
			if (myQueryField == null){
				continue;
			}
			myQueryField.setSelected(theirQueryField.getSelected());
			myQueryField.setOpCode(theirQueryField.getOpCode());
			myQueryField.setOperands(theirQueryField.getOperands());
		}
	}
	
	/**
	 * Merges the list views.
	 * @param workListView contains the list views to merge with.
	 */
	protected void mergeListViews(WorkListView workListView){
		Map<ListType, ListView> myListViews = new HashMap<>();
		for (ListView listView : this.listViews){
			myListViews.put(listView.getType(), listView);
		}
		for (ListView theirListView : workListView.listViews){
			ListView myListView = myListViews.get(theirListView.getType());
			if (myListView != null){
				myListView.setViewPortSize(theirListView.getViewPortSize());
				myListView.setFetchSize(theirListView.getFetchSize());
				myListView.setSelected(theirListView.getSelected());
			} else {
				this.listViews.add(theirListView);
			}
		}
	}
	
	/**
	 * Returns the selected sort options.
	 * @return the selected sort options.
	 */
	@JsonIgnore
	@XmlTransient
	public List<SortOption> getSelectedSortOptions(){
		List<SortOption> sortOptions = new ArrayList<>();
		for (SortOption sortOption : sortInfo.getSortOptions()){
			if (sortOption.getSelected()){
				sortOptions.add(sortOption);
			}
		}
		return sortOptions;
	}
	
	/**
	 * Returns the filters that have selected options.
	 * @return the filters that have selected options.
	 */
	@JsonIgnore
	public List<Filter> getSelectedFilters(){
		List<Filter> selectedFilters = new ArrayList<>();
		for (Filter filter : this.filters){
			List<FilterOption> filterOptions = new ArrayList<>();
			for (FilterOption filterOption : filter.getFilterOptions()){
				if (filterOption.getSelected()){
					filterOptions.add(filterOption);
				}
			}
			if (!filterOptions.isEmpty()){
				Filter selectedFilter = new Filter(filter.getName(), filter.getTitle(), filter.getLink());
				selectedFilter.setFilterOptions(filterOptions);
				selectedFilters.add(selectedFilter);
			}
		}
		return selectedFilters;
	}
	
	/**
	 * Returns the selected list view instance.
	 * @return the selected list view instance.
	 */
	@JsonIgnore
	public ListView getSelectedListView(){
		for (ListView listView : this.listViews){
			if (listView.getSelected()){
				return listView;
			}
		}
		return !this.listViews.isEmpty() ? this.listViews.get(0) : null;
	}
	
	/**
	 * Returns the queries that are selected.
	 * @return the queries that are selected.
	 */
	@JsonIgnore
	public QueryInfo getSelectedQueries(){
		List<QueryField> selectedQueries = new ArrayList<>();
		for (QueryField queryField : this.queryInfo.getQueryFields()){
			if (queryField.getSelected()){
				selectedQueries.add(queryField);
			}
		}
		QueryInfo selectedQueryInfo = new QueryInfo(queryInfo.getName());
		selectedQueryInfo.setQueryFields(selectedQueries);
		return selectedQueryInfo;
	}
	
	/**
	 * @return the list views.
	 */
	@XmlElementWrapper(name="listViews")
	@XmlElements(@XmlElement(name="listView", type=ListView.class))
	@JsonProperty("listViews")
	public List<ListView> getListViews() {
		return listViews;
	}
	/**
	 * @param listViews is the collection of list views.
	 */
	public void setListViews(List<ListView> listViews) {
		this.listViews = listViews;
	}
	
	/**
	 * Get a view for a given type
	 * @param type is the type of List View
	 * @return ListView instane for a given view type
	 */
	public ListView getListView(ListType type){
		for(ListView view: this.getListViews()){
			if(view.getType().equals(type)){
				return view;
			}
		}
		return null;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public boolean equals(Object other){
		if (other instanceof WorkListView){
			WorkListView item = (WorkListView) other;
			return super.equals(other) && EqualityChecker.areEqual(this.filters, item.filters, 
					this.listViews, item.listViews,
					this.queryInfo, item.queryInfo,
					this.sortInfo, item.sortInfo,
					this.index, item.index,
					this.isSaveable, item.isSaveable,
					this.version, item.version);
		} else {
			return false;
		}
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public int hashCode(){
		return super.hashCode() + HashCodeCalculator.calculate(this.filters, 
				this.listViews, 
				this.queryInfo, 
				this.sortInfo, 
				this.index,
				this.isSaveable,
				this.version);
	}
	/**
	 * @return the index name
	 */
	@XmlAttribute(name="index")
	@JsonProperty("index")
	public String getIndex() {
		return index;
	}
	/**
	 * @param index the index name to set
	 */
	public void setIndex(String index) {
		this.index = index;
	}
	
	/**
	 * @return boolean flag indicating if the view can be saved 
	 */
	@XmlAttribute(name="isSaveable")
	@JsonProperty("isSaveable")	
	public boolean isSaveable() {
		return isSaveable;
	}
	
	/**
	 * @param isSaveable is the boolean flag
	 */
	public void setSaveable(boolean isSaveable) {
		this.isSaveable = isSaveable;
	}
	/**
	 * @return the savedSearchInfo
	 */
	@XmlElement(name="savedSearchInfo")
	@JsonProperty("savedSearchInfo")
	public SavedSearchInfo getSavedSearchInfo() {
		return savedSearchInfo;
	}
	/**
	 * @param savedSearchInfo the savedSearchInfo to set
	 */
	public void setSavedSearchInfo(SavedSearchInfo savedSearchInfo) {
		this.savedSearchInfo = savedSearchInfo;
	}
	
	/**
	 * Returns the lookup atom links.
	 * @return the lookup atom links.
	 */
	@XmlElementWrapper(name="lookupLinks")
	@XmlElements(@XmlElement(name="lookupLink", type=AtomLink.class))
	@JsonProperty("lookupLinks")
	public List<AtomLink> getLookupLinks(){
		return lookupLinks ;
	}
	
	/**
	 * Sets the lookup links.
	 * @param lookupLinks is the lookup links to set.
	 */
	public void setLookupLinks(List<AtomLink> lookupLinks){
		this.lookupLinks = lookupLinks;
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public Object clone() throws CloneNotSupportedException {
		WorkListView copy = (WorkListView) super.clone();
		copy.filters = new ArrayList<>(this.filters);
		copy.sortInfo = (SortInfo) this.sortInfo.clone();
		copy.queryInfo = (QueryInfo) this.queryInfo.clone();
		copy.listViews = new ArrayList<>(this.listViews);
		copy.savedSearchInfo = (SavedSearchInfo) this.savedSearchInfo.clone();
		copy.lookupLinks = new ArrayList<>(this.lookupLinks);
		return copy;
	}

}
