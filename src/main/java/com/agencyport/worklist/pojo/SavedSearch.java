/*
 * Created on Feb 5, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The SavedSearch class models the information that is saved when a user saves a set of filters, sort information and additional query parameters.
 * @since 5.1
 */
@XmlRootElement()
public class SavedSearch {
	
	/**
	 * The <code>UNKNOWN_ID</code> is the value for the id to represent a worklist view that has not yet been stored.
	 */
	private static final int UNKNOWN_ID = -1;
	/**
	 * The <code>id</code> is the id of this saved search instance.
	 */
	private int id;
	
	/**
	 * The <code>name</code> is the name of this saved search instance.
	 */
	private String name;
	
	/**
	 * The <code>isActive</code> is set to true for the one and only one search instance which is brought into scope by default.
	 */
	private boolean isActive;
	
	/**
	 * Constructs an instance.
	 */
	public SavedSearch() {
		id = getDefaultSavedSearchId();
		isActive = true;
	}
	
	/**
	 * Constructs an instance.
	 * @param id see {@link #id}
	 * @param name see {@link #name}
	 * @param isActive see {@link #isActive}
	 */
	public SavedSearch(int id, String name, boolean isActive){
		this.id = id;
		this.name = name;
		this.isActive = isActive;
	}
	
	/**
	 * @return the id
	 */
	@XmlAttribute(name="id")
	@JsonProperty("id")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	@XmlAttribute(name="name")
	@JsonProperty("name")
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the isActive flag
	 */
	@XmlAttribute(name="isActive")
	@JsonProperty("isActive")
	public boolean getActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		return "SavedSearch [id=" + id + ", "
				+ "name=" + name + ", " + 
				"isActive=" + isActive + 
				"hasUnknownId=" + isDefaultSavedSearchId(id) + 
				"]";
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public boolean equals(Object other){
		if (other instanceof SavedSearch){
			SavedSearch otherSavedSearch = (SavedSearch) other;
			return EqualityChecker.areEqual(this.id, otherSavedSearch.id,
					this.name, otherSavedSearch.name);
		} else {
			return true;
		}
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public int hashCode(){
		return HashCodeCalculator.calculate(this.id, this.name);
	}
	
	/**
	 * Returns whether the id value represents the value for the default saved search record.  
	 * @param id is the id value.
	 * @return true if the id value represents the value for the default saved search record.
	 */
	public static boolean isDefaultSavedSearchId(int id){
		return id == UNKNOWN_ID;
	}
	
	/**
	 * Returns the id value for the default saved search record. 
	 * @return the id value for the default saved search record.
	 */
	public static int getDefaultSavedSearchId(){
		return UNKNOWN_ID;
	}
}
