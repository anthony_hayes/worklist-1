/**
 * REST API supporting meta data retrieval such as work item and account filter lists. 
 * @since 5.1 
*/
package com.agencyport.worklist.api;