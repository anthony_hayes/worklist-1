package com.agencyport.worklist.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import com.agencyport.api.BaseResource;

/**
 * Expresses a Rest API function which returns a success message if the
 * service is running.
 */
@Path("/heartbeat")
public class HeartbeatFunction extends BaseResource{
    /**
     * Responds with a success message if the service is running
     * @return a success if the service is running, otherwise a 404 message will be returned
     */
    @GET
    public Response getHeartbeat(){
        return this.generateResponse(getAccepts());
    }
}