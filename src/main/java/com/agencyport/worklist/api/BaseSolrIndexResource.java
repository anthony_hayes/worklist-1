/*
 * Created on Mar 14, 2016 by ldeane AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.api;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.solr.common.SolrInputDocument;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

import com.agencyport.api.BaseResource;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.logging.LoggingManager;
import com.agencyport.rest.StandardOutputter;
import com.agencyport.rest.StandardResponseEntity;
import com.agencyport.rest.Status;
import com.agencyport.service.ServiceException;
import com.agencyport.service.xml.DOMUtils;
import com.agencyport.shared.APException;
import com.agencyport.utils.PerfObject;
import com.agencyport.utils.PerfObjectCollector;
import com.agencyport.workitem.impl.WorkItemException;
import com.agencyport.worklist.search.indexing.ISolrIndexer;
import com.agencyport.worklist.searchindex.IDataReader;
import com.agencyport.worklist.searchindex.IndexDataMapper;
import com.agencyport.worklist.searchindex.SearchIndexFactory;
import com.agencyport.worklist.solr.manager.ISolrManager;
import com.agencyport.worklist.solr.manager.SolrManager;

/**
 * 
 * The SolrIndexResource class provides a common derivation point
 * for both account and work item solr resources. 
 */
public class BaseSolrIndexResource extends BaseResource {
	
	/**
	 * The <code>LOGGER</code> is our logger.
	 */
	private static final Logger LOGGER = LoggingManager.getLogger(BaseSolrIndexResource.class.getPackage().getName());
	
	/**
	 * Indexes the work item to search engine.
	 * @param acceptMediaType is the media type.
	 * @param content is the work item content to index.
	 * @param solrIndexer is the associated ISolrIndexer instance.
	 * @return Response to be submitted to the calling entity - this will always be successful as we do not
	 * wish to halt processing just because solr could not index properly.
	 */
	protected Response indexItem(MediaType acceptMediaType, String content, ISolrIndexer solrIndexer) {
		final String functionName = "indexItem";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		JSONObject result = null;
		try {
			InputStream contentStream = new ByteArrayInputStream( content.getBytes(StandardCharsets.UTF_8) );
			Document itemXML = DOMUtils.parse(contentStream);
			ISolrManager solrManager = SolrManager.getSolrManager();
			List<SolrInputDocument> docs = new ArrayList<>();
			docs.add(generateSolrInputDocument(itemXML, solrIndexer));
		
			result = solrManager.index(docs, solrIndexer.getSolrIndexType());
			if(result != null && result.has(SolrManager.SOLR_RESPONSE_HEADER) && 
					result.getJSONObject(SolrManager.SOLR_RESPONSE_HEADER).getInt(SolrManager.SOLR_STATUS) == SolrManager.SOLR_ERROR_STATUS) {
				String error = result.getJSONObject(SolrManager.SOLR_RESPONSE_HEADER).getString(SolrManager.SOLR_ERROR_DESCRIPTION);
				LOGGER.severe("Unable to index work item - " + error);
			}
			
			StandardResponseEntity entry = StandardResponseEntity.create(Status.SUCCESS, result);
			return generateResponse(acceptMediaType, new StandardOutputter(acceptMediaType, new StringWriter(), entry));
		}
		catch(APException | JSONException | ServiceException exception) {
			ExceptionLogger.log(exception, getClass(), functionName);
			return generateExceptionResponse(acceptMediaType, exception, Status.WORK_FLOW_ISSUE, functionName);
		}finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	
	/**
	 * Invokes a commit request to Solr which commits pending documents or updates to make them available for search.
	 * @param acceptMediaType is the media type.
	 * @param solrIndexer is the associated ISolrIndexer instance.
	 * @return Response to be submitted to the calling entity - this will always be successful as we do not
	 * wish to halt processing just because solr could not commit properly.
	 */
	protected Response commitIndex(MediaType acceptMediaType, ISolrIndexer solrIndexer) {
		final String functionName = "commitIndex";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		try {
			ISolrManager solrManager = SolrManager.getSolrManager();
			solrManager.commit(solrIndexer.getSolrIndexType());
			
			StandardResponseEntity entry = StandardResponseEntity.create(Status.SUCCESS);
			return generateResponse(acceptMediaType, new StandardOutputter(acceptMediaType, new StringWriter(), entry));
		}
		catch(APException exception) {
			ExceptionLogger.log(exception, getClass(), functionName);
			return generateExceptionResponse(acceptMediaType, exception, Status.WORK_FLOW_ISSUE, functionName);
		}finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	
	/**
	 * Removes the provided item id from the search engine
	 * @param acceptMediaType is the media type.
	 * @param itemId is the id of the item to be removed.
	 * @param solrIndexer is the associated ISolrIndexer instance.
	 * @return Response to be submitted to the calling entity - this will always be successful as we do not
	 * wish to halt processing just because solr could not remove an index properly.
	 */
	protected Response deleteItemIndex(MediaType acceptMediaType, String itemId, ISolrIndexer solrIndexer) {
		final String functionName = "deleteIndex";
		// Start performance collector for this request
        long performanceTranId = PerfObjectCollector.beginTran();
		PerfObject perfObject = startPerfObject(functionName);
		JSONObject result = null;
		try {
			ISolrManager solrManager = SolrManager.getSolrManager();
			List<String> ids = new ArrayList<>();
			ids.add(itemId);
			result = solrManager.delete(ids, solrIndexer.getSolrIndexType());
			if(result != null && result.has(SolrManager.SOLR_RESPONSE_HEADER) && 
					result.getJSONObject(SolrManager.SOLR_RESPONSE_HEADER).getInt(SolrManager.SOLR_STATUS) == SolrManager.SOLR_ERROR_STATUS) {
				String error = result.getJSONObject(SolrManager.SOLR_RESPONSE_HEADER).getString(SolrManager.SOLR_ERROR_DESCRIPTION);
				LOGGER.severe("Unable to index work item - " + error);
			}
			
			StandardResponseEntity entry = StandardResponseEntity.create(Status.SUCCESS, result);
			return generateResponse(acceptMediaType, new StandardOutputter(acceptMediaType, new StringWriter(), entry));
		}
		catch(APException | JSONException exception) {
			ExceptionLogger.log(exception, getClass(), functionName);
			return generateExceptionResponse(acceptMediaType, exception, Status.WORK_FLOW_ISSUE, functionName);
		}finally {
			perfObject.stop();
			PerfObjectCollector.endTran(performanceTranId);
		}
	}
	
	/**
	 * Constructs the SolrInputDocument from the content XML received.
	 * @param workItemXML is the work item/account XML in Document format.
	 * @param solrIndexer is the associated ISolrIndexer instance
	 * @return SolrInputDocument from the provided content XML.
	 * @throws WorkItemException
	 */
	public final SolrInputDocument generateSolrInputDocument(Document workItemXML, ISolrIndexer solrIndexer) throws WorkItemException{
	    PerfObject perfObject = startPerfObject("generateSolrInputDocument");
		try {
			String solrIndex = solrIndexer.getSolrIndexMapName();
			IDataReader reader = SearchIndexFactory.get().createDataReader(solrIndexer.getWorkItemType());
			IndexDataMapper dataMapper = SearchIndexFactory.get().createIndexDataMapper(solrIndex, reader);
			return (SolrInputDocument) dataMapper.createIndexDocument(workItemXML);			
		} catch (APException apException){
			ExceptionLogger.log(apException, this.getClass(), "generateSolrInputDocument");
			throw new WorkItemException(apException);
		}finally {
            perfObject.stop();
        }
	}
}
