/*
 * Created on Mar 3, 2016 by ldeane AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.factory;

import com.agencyport.factory.GenericFactory;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.shared.APException;
import com.agencyport.workitem.factory.WorkItemFactory;
import com.agencyport.workitem.impl.WorkItemStatusManager;
import com.agencyport.workitem.model.IWorkItemStatusManager;
import com.agencyport.worklist.view.IWorkListViewProvider;
import com.agencyport.worklist.view.WorkListViewProvider;

/**
 * 
 * The WorklistFactory class provides the factory methods for the various work list
 * implementations.
 */
public class WorklistFactory {
	
	/**
	 * Prevent accidental construction.
	 */
	private WorklistFactory(){
		
	}

	/**
	 * Internal creation method.
	 * @param <T> is the type (class) of the instance that will be returned.
	 * @param propertyOverride is the property to find the override for the class name.
	 * @param defaultClassName is the default class name to create an instance on if the property override
	 * is missing from application properties.
	 * @param ctorArgs is the array of constructor parameters.
	 * @return a new instance.
	 */
	@SuppressWarnings("unchecked")
	private static <T> T create(String propertyOverride, String defaultClassName, Object...ctorArgs){
		try {
			return (T) GenericFactory.create(propertyOverride, defaultClassName, ctorArgs);
	    } catch (APException ex ) {
	    	ExceptionLogger.log(ex, WorkItemFactory.class, "create");
	    	throw new IllegalArgumentException(ex);
	    }
	}
	
	/**
	 * Creation method for the IWorkItemStatusManager instance.
	 * @return	An instance of IWorkItemStatusManager.
	 */
	public static IWorkItemStatusManager createWorkItemStatusManager() {
		IWorkItemStatusManager manager = null;
		try {
			manager = GenericFactory.create("workitemstatus.manager_classname", WorkItemStatusManager.class.getName());
		}
		catch(APException e) {
			ExceptionLogger.log(e, WorkItemStatusManager.class, "createWorkItemStatusManager");
			throw new IllegalStateException("Unable to instantiate instance of WorkItemStatusManager.", e);
		}
		
		return manager;
	}
	
	/**
	 * Returns a work list view provider
	 * @return a work list view provider
	 * @since 5.1
	 */
	public static IWorkListViewProvider createWorkListViewProvider(){
		return create("application.work_list_view_provider_classname", WorkListViewProvider.class.getName());
	}

}
