/*
 * Created on Mar 4, 2009 by adoss AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agencyport.factory.GenericFactory;
import com.agencyport.locale.ILocaleConstants;
import com.agencyport.locale.IResourceBundle;
import com.agencyport.locale.ResourceBundleManager;
import com.agencyport.logging.ExceptionLogger;
import com.agencyport.servlets.base.APBaseServlet;
import com.agencyport.servlets.base.APCommand;
import com.agencyport.servlets.base.AccountWorkItemCommand;
import com.agencyport.servlets.base.RequestHelper;
import com.agencyport.servlets.base.WorkItemCommand;
import com.agencyport.utils.text.CharacterEncoding;
import com.agencyport.workitem.model.IWorkListConstants;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.WorkListHelper;
import com.agencyport.worklist.view.CompiledWorkListView;


/**
 * The DisplayWorkInProgress class is the servlet entry for access to the WIP.
 */
public class DisplayWorkInProgress extends APBaseServlet {
   
	/**
	 * The <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 3301609415109579873L;
	
	/**
	 * The <code>DEFAULT_COMMAND_PROPERTY</code> is the default property name used to retrieve the command class for work item command class.
	 */
	private static final String DEFAULT_COMMAND_PROPERTY = "workitem.command_classname";
	
	/** 
	 * {@inheritDoc}
	 */
	@Override
	protected void doPost(HttpServletRequest request, 
      HttpServletResponse response)
      throws ServletException, java.io.IOException {
          processRequest(request, response);
    } 
    
    /** 
     * Processes requests for both HTTP  
     * <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a catastrophic issue has taken place.
     * @throws IOException if resources are not available.
     */
    protected void processRequest(HttpServletRequest request, 
            						HttpServletResponse response)
      				throws ServletException, IOException {
      
		try {
			RequestHelper helper = new RequestHelper(request,false);
			String viewName = WorkListHelper.getWorkListType(helper);
			CompiledWorkListView compiledWorkListView = CompiledWorkListView.get(viewName);
			WorkItemType workItemType = compiledWorkListView.getWorkItemType();
			String defaultClassName = getDefaultClassName(workItemType);

			String commandClassNameProperty = viewName + "." + DEFAULT_COMMAND_PROPERTY;
			
			IResourceBundle coreProductRB = ResourceBundleManager.get().getHTMLEncodedResourceBundle(ILocaleConstants.CORE_PROMPTS_BUNDLE);
			request.setAttribute("CORE_RB", coreProductRB.getEntries());
			
			APCommand apCommand = GenericFactory.create(commandClassNameProperty,defaultClassName);
			apCommand.init(getServletContext(), request, response);
			apCommand.process();
			
		} catch (Exception exception) {
			ExceptionLogger.log(exception, this.getClass(), "processRequest");

			String action = request.getParameter(IWorkListConstants.ACTION);
			if("Copy".equals(action)) {
				setHTTPResponseFields(response,  CharacterEncoding.UTF_8);			
				response.getWriter().append("{\"response\":[{\"error\":\""   +  exception.getMessage() + "\"}]}");
			}else{
				APBaseServlet.displayApplicationErrorPage(getServletContext(), request, response, exception, null);
				response.setHeader("AJAX_ERROR_HEADER", exception.getLocalizedMessage());
			}
			return;
		}
       
    }
    
    /**
     * Get the default command class name for the given work item type for this work list.
     * @param workItemType is the type of work item, account or regular work item.
     * @return the default class name for the given type
     */
    private String getDefaultClassName(WorkItemType workItemType) {
    	String className = WorkItemCommand.class.getName();
    	if(WorkItemType.ACCOUNT_WORK_ITEM_TYPE.equals(workItemType)) {
    		className = AccountWorkItemCommand.class.getName();
    	}
    	return className;
    }
}