/*
 * Created on Dec 8, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.view;

import java.util.List;

import com.agencyport.worklist.pojo.Filter;
import com.agencyport.worklist.pojo.FilterOption;
import com.agencyport.worklist.pojo.QueryInfo;
import com.agencyport.worklist.pojo.SavedSearch;
import com.agencyport.worklist.pojo.SortInfo;
import com.agencyport.worklist.pojo.WorkListView;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.shared.APException;
import com.agencyport.workitem.model.WorkItemType;

/**
 * The IWorkListViewProvider interface supports the serving up of work list filtering, sorting, and searching meta data as well as the access
 * mechanism for performing  work list queries.
 * @since 5.1
 */
public interface IWorkListViewProvider {
	/**
	 * Delivers all of the search filters for a given work item type, view and security profile. The filters need 
	 * to be returned in the order as they should be presented in the UI. 
	 * @param workListViewResource is the compiled worklist view instance.
	 * @param securityProfile is the security profile for the current user.
	 * @return the ordered list of filters.
	 * @throws APException if an issue arose in gathering the filters.
	 */
	List<Filter> getFilters(CompiledWorkListView workListViewResource, ISecurityProfile securityProfile) throws APException;
	
	/**
	 * Delivers the list of options for a given filter.
	 * @param compiledWorkListView is the compiled worklist view instance.
	 * @param filter is the filter. It will only contain the name property. 
	 * @param viewName is the name of the view.
	 * @param securityProfile is the security profile for the current user.
	 * @return the ordered list of filter options.
	 * @throws APException if an issue arose in gathering the filter options.
	 */
	List<FilterOption> getFilterOptions(CompiledWorkListView compiledWorkListView, Filter filter, String viewName, ISecurityProfile securityProfile) throws APException;
	/**
	 * Delivers the sort information for a given work item type, view and security profile.
	 * @param workListViewResource is the compiled worklist view instance.
	 * @param securityProfile is the security profile for the current user.
	 * @return the SortInfo instance.
	 * @throws APException if an issue arose in gathering the sort information.
	 */
	SortInfo getSortInfo(CompiledWorkListView workListViewResource, ISecurityProfile securityProfile) throws APException;
	
	/**
	 * Delivers the query information for a given view.
	 * @param workListViewResource is the compiled worklist view instance.
	 * @param securityProfile is the security profile for the current user.
	 * @return the query information for a given view.
	 * @throws APException if an issue arose gathering the query information.
	 */
	QueryInfo getQueryInfo(CompiledWorkListView workListViewResource, ISecurityProfile securityProfile) throws APException;
	
	/**
	 * Delivers the view. If there is a supporting saved search information record it will be merged in.  
	 * @param workListViewResource is the compiled worklist view instance.
	 * @param workItemType designates whether the request is for a work item or an account.
	 * @param viewName is the name of the view.
	 * @param savedSearchId is the id of the saved search information. A value of {@value com.agencyport.api.worklist.pojo.SavedSearch#UNKNOWN_ID}
	 * implies that the active saved search will be looked up and merged in if the mergeWithSavedView is set to true.
	 * @param securityProfile is the security profile for the current user.
	 * @param mergeWithSavedView controls whether the returned instance is merged with the user's saved version. If this is set to false then
	 * the savedSearchId is ignored.
	 * @return the worklist view.
	 * @throws APException if an issue arose in gathering the view.
	 */
	WorkListView getView(CompiledWorkListView workListViewResource, WorkItemType workItemType, String viewName, int savedSearchId, ISecurityProfile securityProfile, boolean mergeWithSavedView) throws APException;
	
	/**
	 * Retrieves all of the saved search instances for a given user / view name combination.
	 * @param workItemType designates whether the request is for a work item or an account.
	 * @param viewName is the name of the view.
	 * @param securityProfile is the security profile for the current user.
	 * @return  all of the saved search instances for a given user / view name combination.
	 * @throws APException if an issue arose in gathering the saved search instances.
	 */
	List<SavedSearch> getSavedSearches(WorkItemType workItemType, String viewName, ISecurityProfile securityProfile) throws APException;
	
	/**
	 * Saves the work list view instance to the database.
	 * @param workItemType designates whether the request is for a work item or an account.
	 * @param viewName is the name of the view.
	 * @param securityProfile is the security profile for the current user.
	 * @param view is the view to save. This will be updated.
	 * @throws APException if an issue arose trying to persist the search info.
	 */
	void saveView(WorkItemType workItemType, String viewName, ISecurityProfile securityProfile, WorkListView view) throws APException;

	/**
	 * Deletes a saved work list view from database.
	 * @param workItemType designates whether the request is for a work item or an account.
	 * @param viewName is the name of the view.
	 * @param savedSearchId is the id of the saved search information to delete. 	 
	 * @param securityProfile is the security profile for the current user.
	 * @throws APException if an issue arose trying to delete the search info.
	 */
	void deleteView(WorkItemType workItemType, String viewName, int savedSearchId, ISecurityProfile securityProfile) throws APException;
	
	/**
	 * Method for gaining access to the compiled worklist view instance.
	 * @param workItemType is the work item type.
	 * @param viewName is the name of the worklist view.
	 * @param securityProfile is the security profile for the current user.
	 * @return the compiled worklist view instance.
	 * @throws APException if no match for the given viewName or if there is a mismatch between the incoming work 
	 * item type and that found on the compiled work list view instance. 
	 */
	CompiledWorkListView getWorkListViewResource(WorkItemType workItemType, String viewName, ISecurityProfile securityProfile) throws APException;
}
