/*
 * Created on Dec 8, 2014 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.ws.rs.core.MultivaluedMap;

import com.agencyport.api.worklist.pojo.Field;
import com.agencyport.api.worklist.pojo.FilterType;
import com.agencyport.domXML.APDataCollection;
import com.agencyport.logging.LoggingManager;
import com.agencyport.product.ProductDefinitionsManager;
import com.agencyport.rest.AtomLink;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.shared.APException;
import com.agencyport.utils.AppProperties;
import com.agencyport.utils.ArrayHelper;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.text.StringUtilities;
import com.agencyport.version.VersionNumber;
import com.agencyport.workitem.model.WorkItemType;
import com.agencyport.worklist.lookup.ILookupProvider;
import com.agencyport.worklist.lookup.LookupProviderFactory;
import com.agencyport.worklist.lookup.LookupRealm;
import com.agencyport.worklist.lookup.pojo.Code;
import com.agencyport.worklist.lookup.pojo.CodeList;
import com.agencyport.worklist.pojo.Filter;
import com.agencyport.worklist.pojo.FilterOption;
import com.agencyport.worklist.pojo.ListType;
import com.agencyport.worklist.pojo.ListView;
import com.agencyport.worklist.pojo.OpCode;
import com.agencyport.worklist.pojo.QueryField;
import com.agencyport.worklist.pojo.QueryInfo;
import com.agencyport.worklist.pojo.SavedSearch;
import com.agencyport.worklist.pojo.SortInfo;
import com.agencyport.worklist.pojo.SortOption;
import com.agencyport.worklist.pojo.WorkListView;
import com.agencyport.worklist.searchindex.ISearchIndexMapping;

/**
 * The WorkListViewProvider class provides the default implementation of the work list view provider interface. This class can be extended to provide
 * custom solutions for aspects of work list view variability not accounted for in this implementation. 
 * @since 5.1
 */
public class WorkListViewProvider implements IWorkListViewProvider {
	/**
	 * The <code>LOGGER</code> is our own logger.
	 */
	private static final Logger LOGGER = LoggingManager.getLogger(WorkListViewProvider.class.getPackage().getName());
	/**
	 * The <code>WORKITEM_STATUS_FILTER</code> for work item status filtering
	 */
	private static final String WORKITEM_STATUS_FILTER = "application.workitem_status_filter";
	/**
	 * The <code>NO_FILTERS</code> is a reference to an empty filters list.
	 */
	private static final List<Filter> NO_FILTERS = Collections.emptyList();
	/**
	 * The <code>NO_FILTER_OPTIONS</code> is a reference to an empty filter option list.
	 */
	private static final List<FilterOption> NO_FILTER_OPTIONS = Collections.emptyList();
	/**
	 * The FilterOptionComparator class compares the titles of two filter option instances.
	 */
	protected static final class FilterOptionComparator implements Comparator<FilterOption>, Serializable {
		/**
		 * The <code>serialVersionUID</code>
		 */
		private static final long serialVersionUID = -2049887582308172053L;

		/** 
		 * {@inheritDoc}
		 */ 
		@Override
		public int compare(FilterOption o1, FilterOption o2) {
			return o1.getTitle().compareTo(o2.getTitle());
		}
		
	}
	/**
	 * The <code>FILTER_OPTION_COMPARATOR</code> is shared across threads because it is stateless. If it becomes
	 * stateful then this static instance will need to be retired.
	 */
	protected static final FilterOptionComparator FILTER_OPTION_COMPARATOR = new FilterOptionComparator();
	/**
	 * Constructs an instance.
	 */
	public WorkListViewProvider() {
	}
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public List<Filter> getFilters(CompiledWorkListView workListViewResource, ISecurityProfile securityProfile) throws APException {
		APDataCollection workListViewResourceData = workListViewResource.getResourceAsDataCollection(); 
		int[] viewIndex = getViewIndex(workListViewResourceData, securityProfile);
		return getFilters(workListViewResource, workListViewResourceData, viewIndex, securityProfile);
	}

	/** 
	 * {@inheritDoc}
	 */
	@Override
	public List<FilterOption> getFilterOptions(CompiledWorkListView compiledWorkListView, Filter filter, String viewName, ISecurityProfile securityProfile) throws APException {
		List<FilterOption> filterOptions = new ArrayList<>();
		String filterName = filter.getName();
		if (StringUtilities.isEmpty(filterName)){
			throw new APException("Expecting a filter name but none was provided.");
		} 
		FilterType filterType = compiledWorkListView.getFilterType(filterName);
		if (filterType == null){
			throw new APException(String.format("No filter type for filter name '%s', view name '%s' configured in resource '%s'.", filterName, viewName, compiledWorkListView.getResource().getResourceId()));
		}
		LOGGER.info(String.format("Gathering filtering meta data for filter name = '%s', view=%s", filterName, viewName));
		switch (filterType){
		case LOB:
			filterOptions = buildLOBFilterOptions(securityProfile);
			break;
		case STATUS:
			filterOptions = buildStatusFilterOptions(securityProfile);
			break;
		case TRANSACTION_TYPE:
			filterOptions = buildTransactionTypeFilterOptions(securityProfile);
			break;
		case ACCOUNT_TYPE:
			filterOptions = buildAccountTypeFilterOptions(securityProfile);
			break;
		default:
			filterOptions = getCustomFilterOptions(filter, viewName, securityProfile);
			break;
			
		}
		Collections.sort(filterOptions, FILTER_OPTION_COMPARATOR);
		return filterOptions;
	}
	
	/**
	 * Place holder for an application extension to return a set of filter options for a custom filter.
	 * @param filter is the filter.
	 * @param viewName is the name of the work list view.
	 * @param securityProfile is the security profile for the current user.
	 * @return the list of filter options for the given filter.
	 * @throws APException if the filter option build operation was unsuccessful.
	 */
	protected List<FilterOption> getCustomFilterOptions(Filter filter, String viewName, ISecurityProfile securityProfile) throws APException {
		String filterName = filter.getName();
		LOGGER.warning(String.format("Application extension expected for handling filter name='%s', view='%s'", filterName, viewName));
		return NO_FILTER_OPTIONS;
	}
	
	/**
	 * Transforms a CodeList instance to a list of filter options.
	 * @param codeList is the code list.
	 * @param includeSet is the inclusion filter. If this is null then all codes from the code list are transferred. If it isn't
	 * null then include set must contain any {@link Code#getValue()} in order to be included.
	 * @return a List<FilterOption> instance for the given code list.
	 */
	private List<FilterOption> getFilterOptionsFromCodeList(CodeList codeList, Set<String> includeSet) {
		List<FilterOption> filterOptions = new ArrayList<>();
		for (Code code : codeList.getCodes()){
			if (includeSet == null || includeSet.contains(code.getValue())){
				filterOptions.add(FilterOption.create(code.getValue(), code.getTitle()));
			}
		}
		return filterOptions;
	}
	
	/**
	 * Returns the filter options for the LOB filter.
	 * @param securityProfile is the security profile for the current user.
	 * @return the filter options for the LOB filter.
	 * @throws APException propagated from {@link ILookupProvider#get(LookupRealm, String, MultivaluedMap, ISecurityProfile)}
	 */
	protected List<FilterOption> buildLOBFilterOptions(ISecurityProfile securityProfile) throws APException {
		ILookupProvider lookupProvider = LookupProviderFactory.get().getLookupProvider(LookupRealm.BASIC);
		CodeList codeList = lookupProvider.get(LookupRealm.BASIC, FilterType.LOB.name(),null, securityProfile);
		return getFilterOptionsFromCodeList(codeList, null);
	}
	
	/**
	 * Returns the filter options for work item statuses.
	 * @param securityProfile is the security profile for the current user.
	 * @return the filter options for work item statuses.
	 * @throws APException propagated from {@link ILookupProvider#get(LookupRealm, String, MultivaluedMap, ISecurityProfile)}
	 */
	protected List<FilterOption> buildStatusFilterOptions(ISecurityProfile securityProfile) throws APException {
		Set<String> statusesToInclude = new HashSet<>(AppProperties.getAppProperties().getPropertyAsTokens(WORKITEM_STATUS_FILTER, "INPROGRESS;REFER;APPROVE;BIND;REJECT;DECLINE;CLIENTDECLINED", true));
		ILookupProvider lookupProvider = LookupProviderFactory.get().getLookupProvider(LookupRealm.BASIC);
		CodeList codeList = lookupProvider.get(LookupRealm.BASIC, FilterType.STATUS.name(),null, securityProfile);
		return getFilterOptionsFromCodeList(codeList, statusesToInclude);
	}
	
	/**
	 * Returns the filter options for the various transaction types configured in the application.
	 * @param securityProfile is the security profile for the current user.
	 * @return the filter options for the various transaction types configured in the application.
	 * @throws APException propagated from {@link ILookupProvider#get(LookupRealm, String, MultivaluedMap, ISecurityProfile)}
	 */
	protected List<FilterOption> buildTransactionTypeFilterOptions(ISecurityProfile securityProfile) throws APException {
		ILookupProvider lookupProvider = LookupProviderFactory.get().getLookupProvider(LookupRealm.BASIC);
		CodeList codeList = lookupProvider.get(LookupRealm.BASIC, FilterType.TRANSACTION_TYPE.name(),null, securityProfile);
		return getFilterOptionsFromCodeList(codeList, null);
	}
	/**
	 * Returns the filter options for the various account types.
	 * @param securityProfile is the security profile for the current user.
	 * @return the filter options for the various account types.
	 * @throws APException propagated from {@link ILookupProvider#get(LookupRealm, String, MultivaluedMap, ISecurityProfile)}
	 */
	protected List<FilterOption> buildAccountTypeFilterOptions(ISecurityProfile securityProfile) throws APException {
		ILookupProvider lookupProvider = LookupProviderFactory.get().getLookupProvider(LookupRealm.BASIC);
		CodeList codeList = lookupProvider.get(LookupRealm.BASIC, FilterType.ACCOUNT_TYPE.name(),null, securityProfile);
		return getFilterOptionsFromCodeList(codeList, null);
	}
	/** 
	 * {@inheritDoc}
	 */
	@Override
	public SortInfo getSortInfo(CompiledWorkListView workListViewResource, ISecurityProfile securityProfile) throws APException {
		APDataCollection workListViewResourceData = workListViewResource.getResourceAsDataCollection(); 
		int[] viewIndex = getViewIndex(workListViewResourceData, securityProfile);
		return getSortInfo(workListViewResource, workListViewResourceData, viewIndex, securityProfile); 
	}
	
	/**
	 * Routine merges the view that was saved into the default meta data view.  
	 * @param storedView is the view as read from the data store.
	 * @param defaultView is the view as loaded from product configuration.
	 * @return the WorkListView as a result of merged algorithm. 
	 */
	protected WorkListView merge(WorkListView storedView, WorkListView defaultView){
		if (storedView == null){
			return defaultView;
		}
		String storedVersion = storedView.getVersion(); 
		String currentVersion = defaultView.getVersion();
		if (!EqualityChecker.areEqual(storedVersion, currentVersion)){
			// always return the newest version. Users will have to reestablish with each rollout of the software.
			return defaultView;
		}
		defaultView.merge(storedView);
		return defaultView;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public void saveView(WorkItemType workItemType, String viewName, ISecurityProfile securityProfile, WorkListView workListView) throws APException {
		if(!workListView.isSaveable()){
			throw new APException(String.format("View '%s' doesn't support saved searches", workListView.getName()));
		}
		ViewDBProvider dbProvider = new ViewDBProvider();
		dbProvider.save(viewName, securityProfile.getSubject().getId(), workListView);
		workListView.getSavedSearchInfo().setSavedSearches(dbProvider.getSavedSearches(viewName, securityProfile.getSubject().getId()));

	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public void deleteView(WorkItemType workItemType, String viewName, int savedSearchId, ISecurityProfile securityProfile) throws APException {
		ViewDBProvider dbProvider = new ViewDBProvider();
		dbProvider.delete(viewName, savedSearchId, securityProfile.getSubject().getId());
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public QueryInfo getQueryInfo(CompiledWorkListView workListViewResource, ISecurityProfile securityProfile) throws APException {
		APDataCollection workListViewResourceData = workListViewResource.getResourceAsDataCollection(); 
		int[] viewIndex = getViewIndex(workListViewResourceData, securityProfile);
		return getQueryInfo(workListViewResource, workListViewResourceData, viewIndex, securityProfile); 
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public WorkListView getView(CompiledWorkListView workListViewResource, WorkItemType workItemType, String viewName, int savedSearchId, ISecurityProfile securityProfile, boolean mergeWithSavedView) throws APException {
		LOGGER.info(String.format("Gathering work list view info for %s, view='%s'", workItemType, viewName));
		WorkListView defaultView = WorkListView.create(workItemType, viewName);
		VersionNumber currentVersion = ProductDefinitionsManager.getCurrentlyRunningVersion();
		defaultView.setVersion(currentVersion.toString());
		APDataCollection workListViewResourceData = workListViewResource.getResourceAsDataCollection(); 
		int[] viewIndex = getViewIndex(workListViewResourceData, securityProfile);
		defaultView.setIndex(workListViewResourceData.getAttributeText(null, "index"));
		String isSaveableAttr = workListViewResourceData.getFieldValue("@isSaveable","true");
		defaultView.setSaveable(Boolean.parseBoolean(isSaveableAttr));
		List<Filter> filters = getFilters(workListViewResource, workListViewResourceData, viewIndex, securityProfile);
		defaultView.setFilters(filters);
		SortInfo sortInfo = getSortInfo(workListViewResource, workListViewResourceData, viewIndex, securityProfile);
		defaultView.setSortInfo(sortInfo);
		defaultView.setQueryInfo(getQueryInfo(workListViewResource, workListViewResourceData, viewIndex, securityProfile));
		defaultView.setListViews(getListViews(workListViewResource, workListViewResourceData, viewIndex, securityProfile));
		ILookupProvider lookupProvider = LookupProviderFactory.get().getLookupProvider(LookupRealm.BASIC);
		List<AtomLink> lookupLinks = new ArrayList<>();
		lookupProvider.addLinks(lookupLinks, LookupRealm.BASIC);
		addLinksForCustomRealms(lookupLinks);
		defaultView.setLookupLinks(lookupLinks);
		if (mergeWithSavedView && defaultView.isSaveable()){
			ViewDBProvider dbProvider = new ViewDBProvider();
			if (savedSearchId != 0){
				WorkListView usersView = dbProvider.load(viewName, savedSearchId, securityProfile.getSubject().getId());
				if (usersView != null){
					usersView.getSavedSearchInfo().setSavedSearches(dbProvider.getSavedSearches(viewName, securityProfile.getSubject().getId()));
				} else {
					defaultView.getSavedSearchInfo().setSavedSearches(dbProvider.getSavedSearches(viewName, securityProfile.getSubject().getId()));
				}
				return merge(usersView, defaultView);
			} else {
				defaultView.getSavedSearchInfo().setSavedSearches(dbProvider.getSavedSearches(viewName, securityProfile.getSubject().getId()));
				return defaultView;
			}
		} else {
			return defaultView;
		}
	}
	
	/**
	 * Establishes which view entry applies to the current user. For views that have permission elements, the first
	 * view which has one permission that matches one of the permissions in the the current user's permission set will be selected. If there
	 * are no permissions on the views then the first one is returned.
	 * @param workListViewResourceData is the worklist view XML.
	 * @param securityProfile is the security profile for the current user.
	 * @return the index of view to process.
	 * @throws APException if the process of determining the view index failed.
	 */
	protected int[] getViewIndex(APDataCollection workListViewResourceData, ISecurityProfile securityProfile) throws APException{
		int[] noPermissionIndex = null;
		for ( int[] index : workListViewResourceData.createIndexTraversalBasis("views.view")){
			int permissionCount = workListViewResourceData.getCount("views.view.permission", index);
			if (permissionCount == 0){
				noPermissionIndex = index; 
			} else {
				for (int permissionIndex = 0; permissionIndex < permissionCount; permissionIndex++){
					String permissionName = workListViewResourceData.getAttributeText("views.view.permission", index[0], permissionIndex, IConstants.NAME);
					if (securityProfile.getRoles().checkPermission(securityProfile.getSubject().getPrincipal(), permissionName)){
						return index;
					}
				}
			}
		}
		if (noPermissionIndex == null){
			throw new APException("Could not locate work list view for given user profile."); 
		}
		return noPermissionIndex;
	}
	
	/**
	 * Gathers all of the filters for a given view.
	 * @param workListViewResource is the worklist view resource.
	 * @param workListViewResourceData is the worklist view XML.
	 * @param viewIndex is the view index for the view to process.
	 * @param securityProfile is the security profile for the current user.
	 * @return all of the filters for a given view.
	 * @throws APException if the process of determining the appropriate filters failed.
	 */
	protected List<Filter> getFilters(CompiledWorkListView workListViewResource, APDataCollection workListViewResourceData, int[] viewIndex, ISecurityProfile securityProfile) throws APException {
		if (!workListViewResourceData.exists("views.view.filterRefs", viewIndex)){
			return NO_FILTERS;
		}
		ISearchIndexMapping indexMap = workListViewResource.getIndexMap();
		String filtersRefId = workListViewResourceData.getAttributeText("views.view.filterRefs", viewIndex, "filtersRefId");
		List<String> filterRefIds = new ArrayList<>();
		List<Filter> filters = new ArrayList<>();
		if (!StringUtilities.isEmpty(filtersRefId)){
			int[] filtersIndex = workListViewResource.getFiltersMap().get(filtersRefId);
			int filterCount = workListViewResourceData.getCount(IConstants.FILTERS_FILTER_XPATH, filtersIndex);
			for (int filterIndex = 0; filterIndex < filterCount; filterIndex++){
				int[]  combinedIndex = ArrayHelper.append(filtersIndex, filterIndex);
				String filterId = workListViewResourceData.getAttributeText(IConstants.FILTERS_FILTER_XPATH, combinedIndex, "id");
				filterRefIds.add(filterId);
			}
		} else {
			int filterRefCount = workListViewResourceData.getCount("views.view.filterRefs.filterRef", viewIndex);
			for (int filterRefIndex = 0; filterRefIndex < filterRefCount; filterRefIndex++){
				int[]  combinedIndex = ArrayHelper.append(viewIndex, filterRefIndex);
				String filterRefId = workListViewResourceData.getAttributeText("views.view.filterRefs.filterRef", combinedIndex, "filterRefId");
				filterRefIds.add(filterRefId);
			}
		}
		
		for (String filterId : filterRefIds){
			int[] filterIndex = workListViewResource.getFilterMap().get(filterId);
			String filterName = workListViewResourceData.getAttributeText(IConstants.FILTERS_FILTER_XPATH, filterIndex, IConstants.NAME);
			String fieldRefId = workListViewResourceData.getAttributeText(IConstants.FILTERS_FILTER_XPATH, filterIndex, IConstants.FIELD_REF_ID);
			String type = workListViewResourceData.getAttributeText(IConstants.FILTERS_FILTER_XPATH, filterIndex, IConstants.TYPE);
			Field relatedField = indexMap.getField(fieldRefId);
			LocalizedContent content = LocalizedContent.find(workListViewResourceData, IConstants.FILTERS_FILTER_XPATH, filterIndex);
			if (content == null){
				content = indexMap.getLocalizedContent(fieldRefId);
			}
			String saveable = workListViewResourceData.getAttributeText(IConstants.FILTERS_FILTER_XPATH, filterIndex, "isSaveable");
			boolean isSaveable = true;
			if (!StringUtilities.isEmpty( saveable)){
				isSaveable = Boolean.valueOf(saveable);
			}

			Filter filter = Filter.create(filterName, content.getTitle(), workListViewResource.getWorkItemType(), workListViewResource.getViewName());
			filter.setFormat(content.getFormat());
			filter.setStyleClass(content.getStyleClass());
			filter.setRelatedFieldId(relatedField.getId());
			filter.setType(FilterType.valueOf(type));
			filter.setSaveable(isSaveable);
			List<FilterOption> filterOptions = getFilterOptions(workListViewResource, filter, workListViewResource.getViewName(), securityProfile);
			filter.setFilterOptions(filterOptions);
			filters.add(filter);
		}
		return filters;
	}
	
	/**
	 * Retrieves the sort information instance for the given view.
	 * @param workListViewResource is the worklist view resource.
	 * @param workListViewResourceData is the worklist view XML.
	 * @param viewIndex is the view index for the view to process.
	 * @param securityProfile is the security profile for the current user.
	 * @return the sort information instance for the given view.
	 * @throws APException if the process of retrieving the sort information instance failed.
	 */
	protected SortInfo getSortInfo(CompiledWorkListView workListViewResource, APDataCollection workListViewResourceData, int[] viewIndex, ISecurityProfile securityProfile) throws APException {
		if (!workListViewResourceData.exists("views.view.sortInfoRef", viewIndex)){
			return null;
		}
		String sortInfoRefId = workListViewResourceData.getAttributeText("views.view.sortInfoRef", viewIndex, "sortInfoRefId");
		int[] sortInfoIndex = workListViewResource.getSortInfoMap().get(sortInfoRefId);
		String sortInfoName = workListViewResourceData.getAttributeText("sortInfos.sortInfo", sortInfoIndex,  IConstants.NAME);
		LocalizedContent content = LocalizedContent.find(workListViewResourceData, "sortInfos.sortInfo", sortInfoIndex);
		String defaultSortFieldRefId = workListViewResourceData.getAttributeTextIfExists("sortInfos.sortInfo.defaultSortClause", sortInfoIndex,  IConstants.FIELD_REF_ID);
		boolean acsendingFlag = false;
		if (!StringUtilities.isEmpty(defaultSortFieldRefId)){
			String acsending = workListViewResourceData.getAttributeText("sortInfos.sortInfo.defaultSortClause", sortInfoIndex,  "acsending");
			acsendingFlag = Boolean.valueOf(acsending);
		}
		SortInfo sortInfo = SortInfo.create(sortInfoName, content.getTitle(), workListViewResource.getWorkItemType(), workListViewResource.getViewName());
		sortInfo.setSortOptions(getSortOptions(defaultSortFieldRefId, acsendingFlag, sortInfoIndex, workListViewResource, workListViewResourceData, securityProfile));
		return sortInfo;
	}
	/**
	 * Retrieves the sort options for a given sort info segment.
	 * @param defaultSortFieldRefId is the default sort field reference id value.
	 * @param defaultSortFieldAcsendingFlag is the acsending flag for the default sort field.
	 * @param sortInfoIndex specifies which sort info to process.
	 * @param workListViewResource is the worklist view resource.
	 * @param workListViewResourceData is the worklist view XML.
	 * @param securityProfile is the security profile for the current user.
	 * @return the sort options for a given sort info segment.
	 * @throws APException if the process of retrieving the sort options failed.
	 */
	protected List<SortOption> getSortOptions(String defaultSortFieldRefId, boolean defaultSortFieldAcsendingFlag, int[] sortInfoIndex, CompiledWorkListView workListViewResource, APDataCollection workListViewResourceData, ISecurityProfile securityProfile) throws APException {
		int fieldRefCount = workListViewResourceData.getCount(IConstants.SORT_INFOS_SORT_INFO_FIELD_REF_XPATH, sortInfoIndex);
		List<SortOption> sortOptions = new ArrayList<>();
		ISearchIndexMapping indexMap = workListViewResource.getIndexMap();

		for (int fieldRefIndex = 0; fieldRefIndex < fieldRefCount; fieldRefIndex++){
			int[] combinedIndex = ArrayHelper.append(sortInfoIndex, fieldRefIndex);
			String fieldId = workListViewResourceData.getAttributeText(IConstants.SORT_INFOS_SORT_INFO_FIELD_REF_XPATH, combinedIndex, IConstants.FIELD_REF_ID);
			LocalizedContent content = LocalizedContent.find(workListViewResourceData, IConstants.SORT_INFOS_SORT_INFO_FIELD_REF_XPATH, combinedIndex); 
			if (content == null){
				content = indexMap.getLocalizedContent(fieldId);
			}
			String saveable = workListViewResourceData.getAttributeText(IConstants.SORT_INFOS_SORT_INFO_FIELD_REF_XPATH, combinedIndex, "isSaveable");
			boolean isSaveable = true;
			if (!StringUtilities.isEmpty( saveable)){
				isSaveable = Boolean.valueOf(saveable);
			}

			Field field = indexMap.getField(fieldId);
			SortOption sortOption = new SortOption(field.getId(), content.getTitle());
			if (!StringUtilities.isEmpty(defaultSortFieldRefId) && defaultSortFieldRefId.equals(fieldId)){
				sortOption.setAscending(defaultSortFieldAcsendingFlag);
				sortOption.setSelected(true);
			}
			sortOption.setSaveable(isSaveable);
			sortOptions.add(sortOption);
		}
		return sortOptions;
	}
	/**
	 * Retrieves the query information instance for the given view.
	 * @param workListViewResource is the worklist view resource.
	 * @param workListViewResourceData is the worklist view XML.
	 * @param viewIndex is the view index for the view to process.
	 * @param securityProfile is the security profile for the current user.
	 * @return the query information instance for the given view.
	 * @throws APException if the process of retrieving the query information instance failed.
	 */
	protected QueryInfo getQueryInfo(CompiledWorkListView workListViewResource, APDataCollection workListViewResourceData, int[] viewIndex, ISecurityProfile securityProfile) throws APException {
		/**
		 * TODO: Need to figure out how to get worklist transaction information into config service
		 * 		 Logged in Jira: DP-737
		 */		
		
		if (!workListViewResourceData.exists("views.view.queryInfoRef", viewIndex)){
			return null;
		}
		String queryInfoRefId = workListViewResourceData.getAttributeText("views.view.queryInfoRef", viewIndex,  "queryInfoRefId");
		int[] queryInfoIndex = workListViewResource.getQueryInfoMap().get(queryInfoRefId);
		String queryInfoName = workListViewResourceData.getAttributeText(IConstants.QUERY_INFOS_QUERY_INFO_XPATH, queryInfoIndex, IConstants.NAME);
		String transactionId = workListViewResourceData.getAttributeText(IConstants.QUERY_INFOS_QUERY_INFO_XPATH, queryInfoIndex, "transactionId");
		String pageId = workListViewResourceData.getAttributeText(IConstants.QUERY_INFOS_QUERY_INFO_XPATH, queryInfoIndex, "pageId");
		LocalizedContent content = LocalizedContent.find(workListViewResourceData, IConstants.QUERY_INFOS_QUERY_INFO_XPATH, queryInfoIndex);
		QueryInfo queryInfo = QueryInfo.create(queryInfoName, content.getTitle(), workListViewResource.getWorkItemType(), workListViewResource.getViewName());
		
		/*
		queryInfo.setTransactionId(transactionId);
		queryInfo.setPageId(pageId);
		if (!StringUtilities.isEmpty(transactionId)){
			Transaction transaction = TransactionDefinitionManager.getTransaction(transactionId);
			Page page = transaction.getPage(pageId);
			String dialogURI = ProductResource.createPageURI(page);
			queryInfo.getLinks().add(new AtomLink("dialog-resource", dialogURI));
		}*/
		queryInfo.setQueryFields(getQueryFields(workListViewResource, workListViewResourceData, queryInfoIndex, securityProfile));
	
		return queryInfo;
	}
	/**
	 * Retrieves the query fields for a given query information segment. 
	 * @param workListViewResource is the worklist view resource.
	 * @param workListViewResourceData is the worklist view XML.
	 * @param queryInfoIndex is the query information element to process.
	 * @param securityProfile is the security profile for the current user.
	 * @return the query fields for a given query information segment.
	 * @throws APException if the process of retrieving the query fields failed.
	 */
	protected List<QueryField> getQueryFields(CompiledWorkListView workListViewResource, APDataCollection workListViewResourceData, int[] queryInfoIndex, ISecurityProfile securityProfile) throws APException {
		List<QueryField> queryFields = new ArrayList<>();
		
		int queryFieldCount = workListViewResourceData.getCount(IConstants.QUERY_INFOS_QUERY_INFO_QUERY_FIELD_XPATH, queryInfoIndex);
		ISearchIndexMapping indexMap = workListViewResource.getIndexMap();
		for (int queryFieldIndex = 0; queryFieldIndex < queryFieldCount; queryFieldIndex++){
			int[] combinedIndex = ArrayHelper.append(queryInfoIndex, queryFieldIndex);
			String fieldRefId = workListViewResourceData.getAttributeText(IConstants.QUERY_INFOS_QUERY_INFO_QUERY_FIELD_XPATH, combinedIndex, IConstants.FIELD_REF_ID);
			String pageFieldRefId = workListViewResourceData.getAttributeText(IConstants.QUERY_INFOS_QUERY_INFO_QUERY_FIELD_XPATH, combinedIndex, "pageFieldRefId");
			String interactive = workListViewResourceData.getAttributeText(IConstants.QUERY_INFOS_QUERY_INFO_QUERY_FIELD_XPATH, combinedIndex, "interactive");
			boolean isInteractive = true;
			if (!StringUtilities.isEmpty(interactive)){
				isInteractive = Boolean.valueOf(interactive);
			}
			String saveable = workListViewResourceData.getAttributeText(IConstants.QUERY_INFOS_QUERY_INFO_QUERY_FIELD_XPATH, combinedIndex, "isSaveable");
			boolean isSaveable = false;
			if (!StringUtilities.isEmpty( saveable)){
				isSaveable = Boolean.valueOf(saveable);
			}
			Field field = indexMap.getField(fieldRefId);
			QueryField queryField = new QueryField(field.getId(), field.getTitle(), field.getType());
			queryFields.add(queryField);
			queryField.setInteractive(isInteractive);
			queryField.setPageFieldId(pageFieldRefId);
			queryField.setSelected(!isInteractive);
			queryField.setSaveable(isSaveable);
			String opCode = workListViewResourceData.getFieldValue("queryInfos.queryInfo.queryField.opCode", combinedIndex, null);
			if (!StringUtilities.isEmpty(opCode)){
				queryField.setSelected(true);
				queryField.setOpCode(OpCode.valueOf(opCode));
				int operandCount = workListViewResourceData.getCount("queryInfos.queryInfo.queryField.operands.operand", combinedIndex);
				List<String> operands = new ArrayList<>();
				for (int operandIndex = 0; operandIndex < operandCount; operandIndex++){
					int[] combinedIndex2 = ArrayHelper.append(combinedIndex, operandIndex);
					String operand = workListViewResourceData.getFieldValue("queryInfos.queryInfo.queryField.operands.operand", combinedIndex2, null);
					operands.add(operand);
				}
				queryField.setOperands(operands);
			}
		}
		return queryFields;
	}
	/**
	 * Retrieves all of the list views for a given view.
	 * @param workListViewResource is the worklist view resource.
	 * @param workListViewResourceData is the worklist view XML.
	 * @param viewIndex is the view index for the view to process.
	 * @param securityProfile is the security profile for the current user.
	 * @return the list views for a given view.
	 * @throws APException if the process of retrieving the list views failed.
	 */
	protected List<ListView> getListViews(CompiledWorkListView workListViewResource, APDataCollection workListViewResourceData, int[] viewIndex, ISecurityProfile securityProfile) throws APException {
		int listViewCount = workListViewResourceData.getCount(IConstants.VIEWS_VIEW_LIST_VIEW_XPATH, viewIndex);
		List<ListView> listViews = new ArrayList<>();
		for (int listViewIndex = 0; listViewIndex < listViewCount; listViewIndex++){
			int[] combinedIndex = ArrayHelper.append(viewIndex, listViewIndex);
			listViews.add(getListView(workListViewResource, workListViewResourceData, combinedIndex, securityProfile));
		}
		return listViews;
	}
	/**
	 * Retrieves the list view instance for the specified list view .
	 * @param workListViewResource is the worklist view resource.
	 * @param workListViewResourceData is the worklist view XML.
	 * @param listViewIndex is the list view index.
	 * @param securityProfile is the security profile for the current user.
	 * @return the list view instance for the specified list view .
	 * @throws APException if the process of retrieving the list view failed.
	 */
	protected ListView getListView(CompiledWorkListView workListViewResource, APDataCollection workListViewResourceData, int[] listViewIndex, ISecurityProfile securityProfile) throws APException {
		String listViewType = workListViewResourceData.getAttributeText(IConstants.VIEWS_VIEW_LIST_VIEW_XPATH, listViewIndex,  IConstants.TYPE);
		String viewPortSizeValue = workListViewResourceData.getAttributeText(IConstants.VIEWS_VIEW_LIST_VIEW_XPATH, listViewIndex,  "viewPortSize");
		String fetchSizeValue = workListViewResourceData.getAttributeText(IConstants.VIEWS_VIEW_LIST_VIEW_XPATH, listViewIndex,  "fetchSize");
		String selectedValue = workListViewResourceData.getAttributeText(IConstants.VIEWS_VIEW_LIST_VIEW_XPATH, listViewIndex,  "selected");
		int viewPortSize = ListView.DEFAULT_VIEWPORT_SIZE;
		int fetchSize = ListView.DEFAULT_FETCH_SIZE;
		if (!StringUtilities.isEmpty(viewPortSizeValue)){
			viewPortSize = Integer.valueOf(viewPortSizeValue);
		}
		if (!StringUtilities.isEmpty(fetchSizeValue)){
			fetchSize = Integer.valueOf(fetchSizeValue);
		}
		ListView listView = new ListView();
		listView.setViewPortSize(viewPortSize);
		listView.setFetchSize(fetchSize);
		listView.setType(ListType.valueOf(listViewType));
		listView.setSelected(Boolean.valueOf(selectedValue));
		List<Field> fields = new ArrayList<>();
		int fieldRefCount = workListViewResourceData.getCount("views.view.listView.fieldRef", listViewIndex);
		ISearchIndexMapping indexMap = workListViewResource.getIndexMap();
		for (int fieldRefIndex = 0; fieldRefIndex < fieldRefCount; fieldRefIndex++){
			int[] combinedIndex = ArrayHelper.append(listViewIndex, fieldRefIndex);
			String id = workListViewResourceData.getAttributeText(IConstants.VIEWS_VIEW_LIST_VIEW_FIELD_REF_XPATH, combinedIndex, IConstants.FIELD_REF_ID);
			try {
				Field field = indexMap.getField(id);
				field = (Field)field.clone();
				fields.add(field);
				String displayAttr = workListViewResourceData.getAttributeText(IConstants.VIEWS_VIEW_LIST_VIEW_FIELD_REF_XPATH, combinedIndex, IConstants.IS_DISPLAYED);
				boolean isDisplayed = StringUtilities.isEmpty(displayAttr)?true:Boolean.valueOf(displayAttr);
				field.setDisplayed(isDisplayed);
				LocalizedContent content = LocalizedContent.find(workListViewResourceData, IConstants.VIEWS_VIEW_LIST_VIEW_FIELD_REF_XPATH, combinedIndex);
				if(content != null){
					field.setFormat(content.getFormat());
					field.setStyleClass(content.getStyleClass());
					field.setTitle(content.getTitle());
				}
			} catch (CloneNotSupportedException ex) {
				throw new APException(ex);
			}
		}
		listView.setFields(fields);
		
		return listView;
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public List<SavedSearch> getSavedSearches(WorkItemType workItemType, String viewName, ISecurityProfile securityProfile) throws APException {
		ViewDBProvider dbProvider = new ViewDBProvider();
		return dbProvider.getSavedSearches(viewName, securityProfile.getSubject().getId());
	}
	
	/**
	 * Method that should be implemented so a cusstom realm's own lookup links
	 * can be added to the view
	 * @param atomLinks
	 * @throws APException 
	 */
	protected void addLinksForCustomRealms(List<AtomLink> atomLinks) throws APException {
	}
	
	@Override
	public CompiledWorkListView getWorkListViewResource(WorkItemType workItemType, String viewName, ISecurityProfile securityProfile) throws APException {
		CompiledWorkListView workListViewResource = CompiledWorkListView.get(viewName);
		if (!workListViewResource.getWorkItemType().equals(workItemType)){
			throw new APException(String.format("Mismatch of requested view name:%s workItemType:%s with work item type configured:%s ", viewName, workItemType, workListViewResource.getWorkItemType()));
		}
		return workListViewResource;
	}

}
