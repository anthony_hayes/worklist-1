/*
 * Created on Jan 8, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.view;

import com.agencyport.domXML.ElementPathExpression;

/**
 * The ISolrWorkListViewConstants interface contains some XPath expression and other constants relating
 * to the processing of worklist views. 
 * @since 5.1 
 */
public interface IConstants {
	/**
	 * The <code>FIELD_REF_ID</code> is a constant for accessing id attribute values whose name is 'fieldRefId'.
	 */
	String FIELD_REF_ID = "fieldRefId";
	/**
	 * The <code>NAME</code> is a constant for accessing id attribute values whose name is 'name'.
	 */
	String NAME = "name";
	/**
	 * The <code>TYPE</code> is a constant for accessing id attribute values whose name is 'type'.
	 */
	String TYPE = "type";
	/**
	 * The <code>IS_DISPLAYED</code> is a constant for accessing attribute values whose name is 'isDisplayed'.
	 */
	String IS_DISPLAYED = "isDisplayed";
	/**
	 * The <code>FIELD_XPATH</code> is an element path for accessing field elements.
	 */
	ElementPathExpression FIELD_XPATH = new ElementPathExpression("field");
	/**
	 * The <code>FIELD_CONTENT_XPATH</code> is an element path for accessing field and content elements.
	 */
	ElementPathExpression FIELD_CONTENT_XPATH = new ElementPathExpression("field.content");
	/**
	 * The <code>FILTERS_FILTER_XPATH</code> is an element path for accessing filter elements.
	 */
	ElementPathExpression FILTERS_FILTER_XPATH = new ElementPathExpression("filters.filter");
	/**
	 * The <code>QUERY_INFOS_QUERY_INFO_XPATH</code> is an element path for accessing query info elements.
	 */
	ElementPathExpression QUERY_INFOS_QUERY_INFO_XPATH = new ElementPathExpression("queryInfos.queryInfo");
	/**
	 * The <code>QUERY_INFOS_QUERY_INFO_QUERY_FIELD_XPATH</code> is an element path for accessing query field elements.
	 */
	ElementPathExpression QUERY_INFOS_QUERY_INFO_QUERY_FIELD_XPATH = new ElementPathExpression("queryInfos.queryInfo.queryField");
	/**
	 * The <code>VIEWS_VIEW_LIST_VIEW_XPATH</code> is an element path for accessing list view elements.
	 */
	ElementPathExpression VIEWS_VIEW_LIST_VIEW_XPATH = new ElementPathExpression("views.view.listView");
	/**
	 * The <code>VIEWS_VIEW_LIST_VIEW_FIELD_REF_XPATH</code> is an element path for accessing list view field ref elements.
	 */
	ElementPathExpression VIEWS_VIEW_LIST_VIEW_FIELD_REF_XPATH = new ElementPathExpression("views.view.listView.fieldRef");
	/**
	 * The <code>SORT_INFOS_SORT_INFO_FIELD_REF_XPATH</code> is an element path for accessing sort info field ref elements.
	 */
	ElementPathExpression SORT_INFOS_SORT_INFO_FIELD_REF_XPATH = new ElementPathExpression("sortInfos.sortInfo.fieldRef");

}
