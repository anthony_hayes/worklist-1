/*
 * Created on Jan 5, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.view;

import java.util.Locale;

import com.agencyport.domXML.APDataCollection;
import com.agencyport.domXML.ElementPathExpression;
import com.agencyport.id.Id;
import com.agencyport.locale.Locales;
import com.agencyport.utils.ArrayHelper;

/**
 * The LocalizedContent class encapsulates the localized content for various work list view entities.
 * @since 5.1
 */
public final class LocalizedContent {
	/**
	 * The <code>localeId</code> is the locale id for this content.
	 */
	private final Id localeId;
	/**
	 * The <code>title</code> is the title.
	 */
	private final String title;
	/**
	 * The <code>format</code> is the format that should be used to display a value.
	 */
	private final String format;
	/**
	 * The <code>styleClass</code> is the CSS style class to apply to the display.
	 */
	private final String styleClass;
	/**
	 * Constructs an instance.
	 * @param localeId see {@link #localeId}
	 * @param title see {@link #title}
	 * @param format see {@link #format}
	 * @param styleClass see {@link #styleClass}
	 */
	public LocalizedContent(Id localeId, String title, String format, String styleClass){
		this.localeId = localeId;
		this.format = format;
		this.title = title;
		this.styleClass = styleClass;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}
	/**
	 * @return the styleClass
	 */
	public String getStyleClass() {
		return styleClass;
	}

	/**
	 * Searches for the content child element whose localeId attribute values equals the current {@link Locales#getLocaleId()} value.
	 * @param source is the data collection to search.
	 * @param elementPathParent is the element path of the parent of the &lt;content&gt; element.
	 * @param parentIndices contains the indices for the parent element.
	 * @return the LocalizedContent instance matching the locale id. If none is found, then a LocalizedContent for the
	 * default locale will be returned.
	 */
	public static LocalizedContent find(APDataCollection source, ElementPathExpression elementPathParent, int[] parentIndices){
		return find(source, elementPathParent.getExpressionWithAllPredicates(), parentIndices);
	}
	/**
	 * Searches for the content child element whose localeId attribute values equals the current {@link Locales#getLocaleId()} value.
	 * @param source is the data collection to search.
	 * @param elementPathParent is the element path of the parent of the &lt;content&gt; element.
	 * @param parentIndices contains the indices for the parent element.
	 * @return the LocalizedContent instance matching the locale id. If none is found, then a LocalizedContent for the
	 * default locale will be returned.
	 */
	public static LocalizedContent find(APDataCollection source, String elementPathParent, int[] parentIndices){
		LocalizedContent content = find(source, elementPathParent, parentIndices, Locales.get().getLocaleId());
		if (content == null){
			content = find(source, elementPathParent, parentIndices, Locales.get().getLocaleId(Locale.getDefault()));
		}
		return content;
	}
	
	/**
	 * Searches for the content child element whose localeId attribute values equals the current {@link Locales#getLocaleId()} value.
	 * @param source is the data collection to search.
	 * @param elementPathParent is the element path of the parent of the &lt;content&gt; element.
	 * @param parentIndices contains the indices for the parent element.
	 * @param specifiedLocaleId is the locale id to honor.
	 * @return the LocalizedContent instance matching the locale id. Null is returned if none is found.
	 */
	private static LocalizedContent find(APDataCollection source, String elementPathParent, int[] parentIndices, Id specifiedLocaleId){
		ElementPathExpression contentElementPath = new ElementPathExpression(elementPathParent + ".content");
		int count = source.getCount(contentElementPath, parentIndices);
		
		for (int contentIndex = 0; contentIndex < count; contentIndex++){
			int[] combinedIndex = ArrayHelper.append(parentIndices, contentIndex);
			String localeId = source.getAttributeText(contentElementPath, combinedIndex, "localeId");
			if (specifiedLocaleId.asString().equals(localeId)){
				return create(source, contentElementPath, combinedIndex);
			}
		}
		return null;
	}	
	/**
	 * Creates a LocalizedContent instance from its XML source.
	 * @param source is the XML data source. 
	 * @param contentElementPath is the element path of the content element.
	 * @param contentElementPathIndex is the index of the content element.
	 * @return a LocalizedContent instance from its XML source.
	 */
	public static LocalizedContent create(APDataCollection source, ElementPathExpression contentElementPath, int[] contentElementPathIndex){
		String localeIdValue = source.getAttributeText(contentElementPath, contentElementPathIndex, "localeId");
		String title = source.getAttributeText(contentElementPath, contentElementPathIndex, "title");
		String styleClass = source.getAttributeText(contentElementPath, contentElementPathIndex, "styleClass");
		String format = source.getAttributeText(contentElementPath, contentElementPathIndex, "format");
		return new LocalizedContent(new Id(Integer.valueOf(localeIdValue)), title, format, styleClass);
		
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public String toString() {
		return "LocalizedContent [title=" + title + ", "
				+ "format=" + format + 
				", styleClass=" + styleClass +
				", localeId=" + localeId 
				+ "]";
	}
	/**
	 * @return the localeId
	 */
	public Id getLocaleId() {
		return localeId;
	}
}
