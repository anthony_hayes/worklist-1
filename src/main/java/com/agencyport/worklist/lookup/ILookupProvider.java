/*
 * Created on Feb 20, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.lookup;

import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import com.agencyport.worklist.lookup.pojo.CodeList;
import com.agencyport.rest.AtomLink;
import com.agencyport.security.profile.ISecurityProfile;
import com.agencyport.shared.APException;

/**
 * The ILookupProvider interface provides a generic standardized way for looking up various types of codes and their corresponding
 * localized titles.
 * @since 5.1
 */
public interface ILookupProvider {
	/**
	 * Returns the code list for the given name.
	 * @param realm is the realm of the code list.
	 * @param name is the name of the code list.
	 * @param queryParams is the instance of MultivaluesMap that consists of query params and their values
	 * @param securityProfile is the security profile.
	 * @return the code list for the given name.
	 * @throws APException if a resource could not be accessed or the realm was unrecognized.
	 */
	CodeList get(LookupRealm realm, String name, MultivaluedMap<String, String> queryParams, ISecurityProfile securityProfile) throws APException;
	
	/**
	 * Add the atom links for a given realm.
	 * @param atomLinks is the list of atom links to add to.
	 * @param realm is the realm of code lists.
	 * @throws APException if a resource could not be accessed or the realm was unrecognized.
	 */
	 void addLinks(List<AtomLink> atomLinks, LookupRealm realm)throws APException;
}
