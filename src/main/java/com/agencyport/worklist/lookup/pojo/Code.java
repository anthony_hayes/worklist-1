/*
 * Created on Feb 20, 2015 by nbaker AgencyPort Insurance Services, Inc.
 */
package com.agencyport.worklist.lookup.pojo;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import com.agencyport.api.pojo.IKey;
import com.agencyport.utils.EqualityChecker;
import com.agencyport.utils.HashCodeCalculator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * The Code class is simply the aggregation of code value with its localized title.
 * @since 5.1
 */
@XmlRootElement
public class Code implements IKey {
	/**
	 * The <code>title</code> is the title for the filter option.
	 */
	private String title;
	
	/**
	 * The <code>value</code> is the value for the filter option.
	 */
	private String value;
	/**
	 * Constructs an instance.
	 */
	public Code() {
	}
	/**
	 * Constructs an instance.
	 * @param value see {@link #value}
	 * @param title see {@link #title}
	 */
	public Code(String value, String title){
		this.value = value;
		this.title = title;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public boolean equals(Object other){
		if (other instanceof Code){
			Code code = (Code) other;
			return EqualityChecker.areEqual(this.value, code.value,
					this.title, code.title);
					
		} else {
			return false;
		}
	}
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public int hashCode(){
		return HashCodeCalculator.calculate(this.value, this.title);
	}
	
	/** 
	 * {@inheritDoc}
	 */ 
	
	@Override
	public String toString() {
		return Code.class.getSimpleName() + " [value=" + value + 
				", title=" + title +
				"]";
	}
	/**
	 * Gets the title for this filter.
	 * @return the title
	 */
	@XmlAttribute(name="title")
	@JsonProperty("title")
	public String getTitle() {
		return title;
	}
	/**
	 * Sets the title for this item.
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the value
	 */
	@XmlAttribute(name="value")
	@JsonProperty("value")
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/** 
	 * {@inheritDoc}
	 */ 
	@Override
	public String getKey() {
		return value;
	}
	
}
