/**
 * Contains POJO classes for supporting lookup REST services.
 * @since 5.1   
 */
package com.agencyport.worklist.lookup.pojo;